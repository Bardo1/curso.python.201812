<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Python&#xa;Utilizacion" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1547498620324"><hook NAME="MapStyle" zoom="1.282">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#808080ff,#808080ff,#808080ff,#808080ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600.0 px" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="5" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Environment install/isolate" POSITION="left" ID="ID_1854074785" CREATED="1545059350749" MODIFIED="1545059405801">
<edge COLOR="#ffff00"/>
<node TEXT="OS install" ID="ID_1614758151" CREATED="1545059407675" MODIFIED="1545059415132"/>
<node TEXT="virtualenv" ID="ID_643493356" CREATED="1545059415799" MODIFIED="1545059425250"/>
<node TEXT="virtualenvwrapper" ID="ID_1603561560" CREATED="1545059426574" MODIFIED="1545059433570"/>
</node>
<node TEXT="Test" POSITION="left" ID="ID_30164271" CREATED="1545154108115" MODIFIED="1545154112740">
<edge COLOR="#ff00ff"/>
<node TEXT="tox (multi venv)" ID="ID_109158198" CREATED="1545154117033" MODIFIED="1545154146566"/>
<node TEXT="pytest" ID="ID_1842322244" CREATED="1545158644124" MODIFIED="1545158651129"/>
<node TEXT="behave" ID="ID_375180450" CREATED="1546014102371" MODIFIED="1546014105928"/>
</node>
<node TEXT="Usos" POSITION="left" ID="ID_1138097467" CREATED="1545060878402" MODIFIED="1545060886716">
<edge COLOR="#007c7c"/>
<node TEXT="x estructura" ID="ID_1942548052" CREATED="1545060893724" MODIFIED="1545060903671">
<node TEXT="shells" ID="ID_1857194242" CREATED="1545060357239" MODIFIED="1545060909735">
<node TEXT="2.x strings / 3.x bytes / unicode" ID="ID_1475037372" CREATED="1545060380962" MODIFIED="1545060699973"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            run / popen
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            https://stackoverflow.com/questions/4760215/running-shell-command-and-capturing-the-output
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Unicode
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            https://docs.python.org/3/howto/unicode.html
          </p>
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Scripting" ID="ID_1765212579" CREATED="1545060715699" MODIFIED="1545060924375"/>
<node TEXT="Modules" ID="ID_1099206319" CREATED="1545060732678" MODIFIED="1545060928062"/>
<node TEXT="Libs" ID="ID_300644332" CREATED="1545060852958" MODIFIED="1545060930932"/>
</node>
<node TEXT="Utilizaci&#xf3;n" ID="ID_1690433481" CREATED="1545061259589" MODIFIED="1545061272572">
<node TEXT="Prototipado" ID="ID_1388996845" CREATED="1545061221122" MODIFIED="1545061240841"/>
<node TEXT="Integraci&#xf3;n" ID="ID_1812014396" CREATED="1545061286669" MODIFIED="1545061293800">
<node TEXT="Python" ID="ID_1103839017" CREATED="1545061294629" MODIFIED="1545061299223"/>
<node TEXT="C" ID="ID_249068670" CREATED="1545064590247" MODIFIED="1545064593470"/>
<node TEXT="JVM" ID="ID_656837530" CREATED="1545061299753" MODIFIED="1545061305216"/>
</node>
<node TEXT="Performance?" ID="ID_425493421" CREATED="1545061247113" MODIFIED="1550371175066"/>
</node>
</node>
<node TEXT="Orientaci&#xf3;n" POSITION="left" ID="ID_350841371" CREATED="1545060987942" MODIFIED="1545060993899">
<edge COLOR="#7c7c00"/>
<node TEXT="OOP" ID="ID_1710338019" CREATED="1545060994918" MODIFIED="1545061021890"/>
<node TEXT="Functional" ID="ID_1559229803" CREATED="1545060999489" MODIFIED="1545061008602"/>
<node TEXT="Mixed" ID="ID_197514059" CREATED="1545061010223" MODIFIED="1545061014927"/>
</node>
<node TEXT="Lenguaje" POSITION="left" ID="ID_185822762" CREATED="1545061419785" MODIFIED="1545061432716">
<edge COLOR="#ff0000"/>
<node TEXT="Declaraciones" ID="ID_1048885350" CREATED="1545061440998" MODIFIED="1545061457480"/>
<node TEXT="OO" ID="ID_611521152" CREATED="1545061458182" MODIFIED="1545061682582">
<node TEXT="Class info" ID="ID_1258740891" CREATED="1545061577212" MODIFIED="1545064366514"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <td valign="top" style="width: 66%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clases
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 66%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Herencia
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 66%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Instancias
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 66%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Atributos
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 66%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            M&#233;todos
          </p>
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="FP" ID="ID_640996227" CREATED="1545061693587" MODIFIED="1545061696792"/>
</node>
<node TEXT="Keyword-only arguments" POSITION="left" ID="ID_97477445" CREATED="1545326115025" MODIFIED="1545326115027">
<edge COLOR="#808080"/>
</node>
</node>
</map>
