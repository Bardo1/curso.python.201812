<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Python b&#xe1;sico" FOLDED="false" ID="ID_191153586" CREATED="1547495961950" MODIFIED="1552670991220" ICON_SIZE="36.0 pt" LINK="https://docs.python.org/3/library/stdtypes.html" STYLE="oval">
<font SIZE="22" BOLD="true"/>
<hook NAME="MapStyle" zoom="0.826">
    <properties fit_to_viewport="false" show_note_icons="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" backgroundImageURI="../../logo/python-logo-generic.svg"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" ICON_SIZE="64.0 pt" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Segoe Print" SIZE="22"/>
<edge COLOR="#ffffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" ICON_SIZE="32.0 px" COLOR="#000000" BACKGROUND_COLOR="#ffffcc" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="2.6 pt" SHAPE_VERTICAL_MARGIN="2.6 pt" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="18" BOLD="false" ITALIC="true"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" ICON_SIZE="28.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="16"/>
<edge STYLE="bezier" WIDTH="4"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" ICON_SIZE="24.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="14"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" ICON_SIZE="24.0 px" COLOR="#111111" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="13"/>
<edge STYLE="bezier" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" ICON_SIZE="24.0 px" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="12"/>
<edge STYLE="bezier" WIDTH="1"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" ICON_SIZE="24.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" ICON_SIZE="16.0 px">
<font SIZE="10"/>
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="36" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<edge COLOR="#ffffff"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<hook URI="python-logo-generic-11-2.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Herramientas" POSITION="right" ID="ID_273436263" CREATED="1547495961959" MODIFIED="1553809578463" LINK="Python-Herramientas.mm" HGAP_QUANTITY="91.26829627750192 pt" VSHIFT_QUANTITY="-30.731708746733723 pt">
<edge COLOR="#007c7c"/>
<node TEXT="Int&#xe9;rprete" ID="ID_1739129483" CREATED="1551539904812" MODIFIED="1551539914353">
<node TEXT="python2" ID="ID_1121912883" CREATED="1551539915890" MODIFIED="1551539930083">
<node TEXT="idle" ID="ID_1888810102" CREATED="1551540052083" MODIFIED="1551540055111"/>
</node>
<node TEXT="python3" ID="ID_738763995" CREATED="1551539931090" MODIFIED="1551539935768">
<node TEXT="ipython" ID="ID_1729867477" CREATED="1551539937043" MODIFIED="1551539946504"/>
</node>
</node>
<node TEXT="IDEs" ID="ID_613301783" CREATED="1547495961959" MODIFIED="1551465157084" HGAP_QUANTITY="91.26829627750192 pt" VSHIFT_QUANTITY="-30.731708746733723 pt">
<node TEXT="SublimeText&#xa;+ packages" ID="ID_125955980" CREATED="1551965796015" MODIFIED="1552395342057"/>
<node TEXT="MS VSCode&#xa;+extensions" ID="ID_833699200" CREATED="1551457067539" MODIFIED="1552395377937"/>
<node TEXT="Pycharm&#xa;CE / Pro" ID="ID_1637908174" CREATED="1551457074870" MODIFIED="1552395430102"/>
</node>
<node TEXT="notebook" ID="ID_348011905" CREATED="1551457019628" MODIFIED="1552998490454" LINK="http://localhost:8888/tree">
<node TEXT="ipython" ID="ID_604228650" CREATED="1551457085552" MODIFIED="1552998486587"/>
<node TEXT="jupyter (anaconda)" ID="ID_1643422030" CREATED="1551457099728" MODIFIED="1552998483053" LINK="https://jupyter.readthedocs.io/en/latest/install.html"/>
</node>
</node>
<node TEXT="Tips iniciales" POSITION="right" ID="ID_634869024" CREATED="1551888698280" MODIFIED="1551888837109" LINK="http://localhost:8888/tree/PYTHON_basico">
<edge COLOR="#00ffff"/>
<node TEXT="print Py2/Py3" FOLDED="true" ID="ID_1379999045" CREATED="1547495961959" MODIFIED="1552396450276" LINK="http://localhost:8888/notebooks/compatibilizar_Py2_future.ipynb" HGAP_QUANTITY="91.26829627750192 pt" VSHIFT_QUANTITY="-30.731708746733723 pt">
<node TEXT="PY2 print es sentencia" ID="ID_1951009310" CREATED="1551889166052" MODIFIED="1551889214162" LINK="http://localhost:8888/notebooks/PYTHON_basico/000%20PY2%20print.ipynb"/>
<node TEXT="PY3: print() esfunci&#xf3;n" ID="ID_1490224175" CREATED="1551889190453" MODIFIED="1551889207524" LINK="http://localhost:8888/notebooks/PYTHON_basico/000%20PY3%20print.ipynb"/>
<node TEXT="from future" ID="ID_1051570213" CREATED="1551889269283" MODIFIED="1551889287775" LINK="http://localhost:8888/notebooks/PYTHON_basico/000%20PY2%20-%20PY3%20from%20future.ipynb"/>
<node TEXT="diferencia entre&#xa; sentencia y funci&#xf3;n" ID="ID_1579483140" CREATED="1551889305621" MODIFIED="1552322603109" LINK="http://localhost:8888/notebooks/compatibilizar_Py2_future.ipynb"/>
<node TEXT="SIX" ID="ID_1871038738" CREATED="1551889396220" MODIFIED="1551889409945" LINK="http://localhost:8888/notebooks/compatibilizar_Six_Py2_Py3.ipynb"/>
</node>
<node TEXT="Ayuda en l&#xed;nea" FOLDED="true" ID="ID_1441481189" CREATED="1551890935159" MODIFIED="1551890942017">
<node TEXT="help(__future__)" ID="ID_846969476" CREATED="1551890943238" MODIFIED="1551890950878"/>
</node>
<node TEXT="introspecci&#xf3;n" FOLDED="true" ID="ID_1424196428" CREATED="1551889329776" MODIFIED="1551889336763">
<node TEXT="dir()" ID="ID_877076592" CREATED="1551889343981" MODIFIED="1551889348471"/>
<node TEXT="id()" ID="ID_221975349" CREATED="1551896530138" MODIFIED="1551896533561"/>
<node TEXT=".__doc__" ID="ID_119056480" CREATED="1552326949743" MODIFIED="1552326958118"/>
</node>
<node TEXT="Indentado" ID="ID_1552588969" CREATED="1551892123365" MODIFIED="1551892549689" LINK="http://localhost:8888/notebooks/PYTHON_basico/001%20Indentado.ipynb"/>
<node TEXT="Codificaci&#xf3;n" FOLDED="true" ID="ID_1430965245" CREATED="1551892698230" MODIFIED="1551892703966">
<node TEXT="en general" ID="ID_1223204225" CREATED="1551892803708" MODIFIED="1552327687196" LINK="http://localhost:8888/notebooks/PYTHON_basico/002%20Codificaci%C3%B3n.ipynb"/>
<node TEXT="en scripts&apos;" ID="ID_208781991" CREATED="1551892812898" MODIFIED="1552327674314" LINK="file:/home/Neoris/curso.python.201812.git/curso.python.201812/Curso.py3/000-000-presentacion.py"/>
</node>
</node>
<node TEXT="Objetos" POSITION="right" ID="ID_919167013" CREATED="1551463889841" MODIFIED="1553809768138" LINK="Objetos.mm">
<edge COLOR="#ff00ff"/>
</node>
<node TEXT="Funciones" POSITION="right" ID="ID_1774523166" CREATED="1553002193870" MODIFIED="1553024214564" LINK="http://localhost:8888/notebooks/100-Funciones.ipynb">
<edge COLOR="#ff0000"/>
<node TEXT="argumentos" ID="ID_1120053564" CREATED="1553788443392" MODIFIED="1553788469244">
<node TEXT="posicionales" ID="ID_930989247" CREATED="1553788471857" MODIFIED="1553788476737">
<node TEXT="obligatorios" ID="ID_1023388218" CREATED="1553788488932" MODIFIED="1553788493392"/>
<node TEXT="opcionales" ID="ID_1598478194" CREATED="1553788493994" MODIFIED="1553788498094"/>
</node>
<node TEXT="nombrados" ID="ID_234113127" CREATED="1553788477788" MODIFIED="1553788486358"/>
<node TEXT="posicionales adicionales" ID="ID_1832790466" CREATED="1553788511185" MODIFIED="1553788526155"/>
<node TEXT="nombraados adicionales" ID="ID_1567472205" CREATED="1553788527606" MODIFIED="1553788533184"/>
</node>
</node>
<node TEXT="m&#xe1;s de StdLib" POSITION="right" ID="ID_172328910" CREATED="1552322234187" MODIFIED="1552331522893">
<edge COLOR="#7c0000"/>
<node TEXT="Versi&#xf3;n Python y ayudas" ID="ID_744836077" CREATED="1552322254229" MODIFIED="1552331611803" LINK="http://localhost:8888/notebooks/00-REPL/00-00-sys-version.ipynb"/>
<node TEXT="Argumentos" FOLDED="true" ID="ID_942918139" CREATED="1552324010429" MODIFIED="1552324109979">
<node TEXT="sys" ID="ID_1934381159" CREATED="1552324017993" MODIFIED="1552324041216" LINK="http://localhost:8888/notebooks/00-REPL/00-00-sys-version.ipynb"/>
<node TEXT="platform" ID="ID_1087701047" CREATED="1552324060832" MODIFIED="1552324065694"/>
</node>
</node>
<node TEXT="Organizaci&#xf3;n" POSITION="left" ID="ID_1119755622" CREATED="1547498496749" MODIFIED="1552999629720" LINK="Organizacion.mm">
<edge COLOR="#00ffff"/>
</node>
<node TEXT="Orientaci&#xf3;n" POSITION="left" ID="ID_1680205590" CREATED="1552999615363" MODIFIED="1552999705529" LINK="Orientacion.mm">
<edge COLOR="#7c7c00"/>
</node>
<node TEXT="Estructuras de control" POSITION="left" ID="ID_624754895" CREATED="1555069986468" MODIFIED="1555070172886" LINK="http://localhost:8888/notebooks/PYTHON_basico/100-Estructutas-de-control.ipynb">
<edge COLOR="#00ff00"/>
</node>
</node>
</map>
