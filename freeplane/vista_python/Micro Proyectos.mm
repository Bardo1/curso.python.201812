<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Micro&#xa;Proyectos" FOLDED="false" ID="ID_191153586" CREATED="1553174154885" MODIFIED="1553174167129" ICON_SIZE="36.0 pt" LINK="menuitem:_ExternalImageAddAction" STYLE="oval">
<font SIZE="22"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" ICON_SIZE="64.0 pt" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Segoe Print" SIZE="22"/>
<edge COLOR="#ffffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" ICON_SIZE="32.0 px" COLOR="#000000" BACKGROUND_COLOR="#ffffcc" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="2.6 pt" SHAPE_VERTICAL_MARGIN="2.6 pt" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="18" BOLD="false" ITALIC="true"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" ICON_SIZE="28.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="16"/>
<edge STYLE="bezier" WIDTH="4"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" ICON_SIZE="24.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="14"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" ICON_SIZE="24.0 px" COLOR="#111111" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="13"/>
<edge STYLE="bezier" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" ICON_SIZE="24.0 px" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="12"/>
<edge STYLE="bezier" WIDTH="1"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" ICON_SIZE="24.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" ICON_SIZE="16.0 px">
<font SIZE="10"/>
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="17" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<edge COLOR="#ffffff"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      1. Write the central idea
    </p>
    <p>
      2. Save the map
    </p>
    <p>
      3. Select central node,
    </p>
    <p>
      &#160;&#160;&#160;&#160;click on icon to add picture
    </p>
    <p>
      4. Delete menu links Add image..
    </p>
    <p>
      &#160;&#160;&#160;&#160;with CTRL+K and delete field with link
    </p>
    <p>
      5. Delete node details with right click on mouse
    </p>
    <p>
      &#160;&#160;&#160;&#160;on central node - Remove node details
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Pseudo Neoris apps" POSITION="right" ID="ID_96778937" CREATED="1553174330429" MODIFIED="1553174340579">
<edge COLOR="#00ffff"/>
<node TEXT="Vacation" ID="ID_273436263" CREATED="1553174154903" MODIFIED="1553174344404" HGAP_QUANTITY="91.26829627750192 pt" VSHIFT_QUANTITY="-30.731708746733723 pt">
<node TEXT="IDEA 1.1" ID="ID_588194298" CREATED="1553174154904" MODIFIED="1553174154904" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-12.292683498693489 pt"/>
<node TEXT="IDEA 1.2" ID="ID_1104103844" CREATED="1553174154904" MODIFIED="1553174154904"/>
</node>
<node TEXT="Lawson" ID="ID_495676614" CREATED="1553174154905" MODIFIED="1553174347362" HGAP_QUANTITY="101.80488213352491 pt" VSHIFT_QUANTITY="-42.14634342409195 pt">
<node TEXT="IDEA 2.1" ID="ID_744391648" CREATED="1553174154905" MODIFIED="1553174154905" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-14.926829962699236 pt"/>
<node TEXT="IDEA 2.2" ID="ID_903669612" CREATED="1553174154905" MODIFIED="1553174154905"/>
</node>
</node>
<node TEXT="GSheets" POSITION="right" ID="ID_1041579130" CREATED="1553176383311" MODIFIED="1553176387608">
<edge COLOR="#7c0000"/>
<node TEXT="Toma de datos" ID="ID_1492009312" CREATED="1553176388863" MODIFIED="1553176394054"/>
<node TEXT="Impacta informe" ID="ID_163473307" CREATED="1553176395375" MODIFIED="1553176401748"/>
</node>
<node TEXT="Blockchain" POSITION="right" ID="ID_90823870" CREATED="1553174154906" MODIFIED="1553174186228" HGAP_QUANTITY="56.146343424091945 pt" VSHIFT_QUANTITY="-61.46341749346744 pt">
<edge COLOR="#ff0000"/>
<node TEXT="Vacation contract" ID="ID_1201198400" CREATED="1553174154906" MODIFIED="1553174204785" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-15.804878784034486 pt"/>
<node TEXT="Project hours  contract" ID="ID_1615683347" CREATED="1553174154909" MODIFIED="1553174218314" HGAP_QUANTITY="17.512195285340997 pt" VSHIFT_QUANTITY="3.512195285340997 pt"/>
</node>
<node TEXT="Resources for projects" POSITION="left" ID="ID_25434398" CREATED="1553174154909" MODIFIED="1553174264632" HGAP_QUANTITY="73.70731985079694 pt" VSHIFT_QUANTITY="-37.75609931741573 pt">
<edge COLOR="#cc00ff"/>
<node TEXT="AI absence prediction" ID="ID_352937383" CREATED="1553174154910" MODIFIED="1553174301073" HGAP_QUANTITY="13.121951178664752 pt" VSHIFT_QUANTITY="-9.658537034687741 pt"/>
<node TEXT="IDEA 6.2" ID="ID_966834988" CREATED="1553174154910" MODIFIED="1553174154910" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-7.902439392017243 pt"/>
</node>
<node TEXT="IDEA 5" POSITION="left" ID="ID_1822402846" CREATED="1553174154910" MODIFIED="1553174154910" HGAP_QUANTITY="84.24390570681993 pt" VSHIFT_QUANTITY="-36.00000167474521 pt">
<edge COLOR="#ff9900"/>
<node TEXT="IDEA 5.1" ID="ID_1274507163" CREATED="1553174154911" MODIFIED="1553174154911" HGAP_QUANTITY="14.0 pt" VSHIFT_QUANTITY="-21.07317171204598 pt"/>
<node TEXT="IDEA 5.2" ID="ID_1203076641" CREATED="1553174154911" MODIFIED="1553174154911"/>
</node>
<node TEXT="IDEA 4" POSITION="left" ID="ID_575079876" CREATED="1553174154911" MODIFIED="1553174154911" HGAP_QUANTITY="67.56097810145019 pt" VSHIFT_QUANTITY="-42.14634342409195 pt">
<edge COLOR="#0000ff"/>
<node TEXT="IDEA 4.1" ID="ID_317662625" CREATED="1553174154911" MODIFIED="1553174154911" HGAP_QUANTITY="14.878048821335248 pt" VSHIFT_QUANTITY="-18.439025248040235 pt"/>
<node TEXT="IDEA 4.2" ID="ID_1522387997" CREATED="1553174154911" MODIFIED="1553174154911"/>
</node>
</node>
</map>
