<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Orientaci&#xf3;n" FOLDED="false" ID="ID_191153586" CREATED="1546988829764" MODIFIED="1553000246874" ICON_SIZE="36.0 pt" STYLE="oval">
<font SIZE="22"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" ICON_SIZE="64.0 pt" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Segoe Print" SIZE="22"/>
<edge COLOR="#ffffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" ICON_SIZE="32.0 px" COLOR="#000000" BACKGROUND_COLOR="#ffffcc" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="2.6 pt" SHAPE_VERTICAL_MARGIN="2.6 pt" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="18" BOLD="false" ITALIC="true"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" ICON_SIZE="28.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="16"/>
<edge STYLE="bezier" WIDTH="4"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" ICON_SIZE="24.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="14"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" ICON_SIZE="24.0 px" COLOR="#111111" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="13"/>
<edge STYLE="bezier" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" ICON_SIZE="24.0 px" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="12"/>
<edge STYLE="bezier" WIDTH="1"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" ICON_SIZE="24.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" ICON_SIZE="16.0 px">
<font SIZE="10"/>
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="27" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<edge COLOR="#ffffff"/>
<hook URI="python-logo-generic-11-2.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Codificando..." POSITION="right" ID="ID_425552906" CREATED="1552999493132" MODIFIED="1552999503834">
<edge COLOR="#00ffff"/>
<node TEXT="inmediato" ID="ID_575079876" CREATED="1546988829777" MODIFIED="1552999513054" HGAP_QUANTITY="67.56097810145019 pt" VSHIFT_QUANTITY="-42.14634342409195 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">import sys</font>
    </p>
    <p>
      <font size="4">print sys.path</font>
    </p>
  </body>
</html>
</richcontent>
<font SIZE="18"/>
</node>
<node TEXT="script" ID="ID_1968973655" CREATED="1547469042746" MODIFIED="1552999517719"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      import sys
    </p>
    <p>
      
    </p>
    <p>
      print sys.path
    </p>
  </body>
</html>
</richcontent>
<node TEXT="file.ext" ID="ID_273436263" CREATED="1546988829774" MODIFIED="1549295287002" HGAP_QUANTITY="91.26829627750192 pt" VSHIFT_QUANTITY="-30.731708746733723 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b><font size="4">Usuales</font></b>
    </p>
    <p>
      <font size="4">.py&#160;&#160;&#160;&#160;&#160;source code </font>
    </p>
    <p>
      <font size="4">.pyc&#160;&#160;&#160;&#160;compiled bytecode </font>
    </p>
    <p>
      <font size="4">.pyo&#160;&#160;&#160;&#160;pre Python 3.5 : bytecode &quot;optimized&quot; (-O) </font>
    </p>
    <p>
      <font size="4">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;- assert </font>
    </p>
    <p>
      <font size="4">.pyd&#160;&#160;&#160;&#160;basically a windows dll file</font>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="m&#xf3;dulo" ID="ID_900075296" CREATED="1547469083496" MODIFIED="1552999537320" LINK="Modules_define.mm">
<node TEXT="import from MODULE" ID="ID_495676614" CREATED="1546988829775" MODIFIED="1549296340841" HGAP_QUANTITY="101.80488213352491 pt" VSHIFT_QUANTITY="-42.14634342409195 pt">
<node TEXT="M&#xf3;dulo simple" ID="ID_744391648" CREATED="1546988829775" MODIFIED="1549295918230" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-14.926829962699236 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4"># any file with code is a module </font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4">import os </font>
    </p>
    <p>
      <font size="4">print os.sep</font>
    </p>
  </body>
</html>
</richcontent>
<font SIZE="14"/>
</node>
<node TEXT="SubM&#xf3;dulos" ID="ID_903669612" CREATED="1546988829775" MODIFIED="1549296414604"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">Jerarqu&#237;a de carpetas </font>
    </p>
    <p>
      <b><font size="4">sin __init__.py</font></b>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Namespacing" ID="ID_1561680840" CREATED="1549295255898" MODIFIED="1549295268873"/>
<node TEXT="Uso como MODULO o SCRIPT" ID="ID_332717920" CREATED="1549297196218" MODIFIED="1549300046689"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">#!/usr/bin/env python3 </font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4"># ejemplos/modulos_001.py </font>
    </p>
    <p>
      <font size="4">import os </font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4">def mi_path(): </font>
    </p>
    <p>
      <font size="4">&#160;&#160;nvo_path=&quot;aa&quot;+os.sep+&quot;bb&quot; </font>
    </p>
    <p>
      <font size="4">&#160;&#160;return nvo_path </font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4">if __name__=&quot;__main__&quot;: </font>
    </p>
    <p>
      <font size="4">&#160;&#160;print mi_path() </font>
    </p>
    <p>
      
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="packages" ID="ID_942167910" CREATED="1549295218913" MODIFIED="1552999541641">
<node TEXT="Jerarqu&#xed;a de carpetas&#xa;+ m&#xf3;dulos&#xa;+ __init__.py" ID="ID_1501478608" CREATED="1549295926993" MODIFIED="1549296006103"/>
<node TEXT="Carpetas =&gt; visibilidad (namespacing)" ID="ID_459145747" CREATED="1549296015034" MODIFIED="1549296038141"/>
<node TEXT="Archivos.py =&gt; M&#xf3;dulos componentes del package" ID="ID_1632146113" CREATED="1549296040385" MODIFIED="1549296064511"/>
<node TEXT="Jerarqu&#xed;a:&#xa;import my-pack&#xa;import my-pack.my_mod1&#xa;import my-pack.my_mod1.mod1a&#xa;import my-pack.my_mod1.mod1b&#xa;import my-pack.my_mod2.modda&#xa;import my-pack.my_mod2.mod2b" ID="ID_657451311" CREATED="1549296066038" MODIFIED="1549296170127"/>
<node TEXT="__init__py:&#xa;version&#xa;imports" ID="ID_572746626" CREATED="1549296173966" MODIFIED="1549296241882"/>
</node>
<node TEXT="librer&#xed;a" ID="ID_90823870" CREATED="1546988829775" MODIFIED="1552999551940" HGAP_QUANTITY="56.146343424091945 pt" VSHIFT_QUANTITY="-61.46341749346744 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">conjunto de m&#243;dulos/packages</font>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Packages vs modules" POSITION="right" ID="ID_1121819438" CREATED="1549296462383" MODIFIED="1552591813315" LINK="https://stackoverflow.com/questions/7948494/whats-the-difference-between-a-python-module-and-a-python-package">
<edge COLOR="#7c7c00"/>
<richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Any module that contains a __path__ attribute is considered a package</b>.
    </p>
    <p>
      import xx
    </p>
    <p>
      &#160;de module o package es SIEMPRE
    </p>
    <p>
      type(xx) == module
    </p>
    <p>
      
    </p>
    <p>
      Package import
    </p>
    <p>
      Visibles seg&#250;n __init__.py
    </p>
    <p>
      
    </p>
    <p>
      Ejemplo: # https://stackoverflow.com/questions/7948494/whats-the-difference-between-a-python-module-and-a-python-package
    </p>
    <p>
      
    </p>
    <p>
      $ mkdir -p a/b
    </p>
    <p>
      $ touch a/b/c.py
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      import a
    </p>
    <p>
      a&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#8658; &lt;module 'a' (namespace)&gt;
    </p>
    <p>
      a.b&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#8658; AttributeError: module 'a' has no attribute 'b'
    </p>
    <p>
      import a.b.c
    </p>
    <p>
      a.b&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#8658; &lt;module 'a.b' (namespace)&gt;
    </p>
    <p>
      a.b.c&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#8658; &lt;module 'a.b.c' from '/home/cjs/a/b/c.py'&gt;
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="1. Sentencia&#xa;2. L&#xed;nea de comando&#xa;3. M&#xf3;dulo (Archivo fuente)&#xa;4. Librer&#xed;a&#xa;5. M&#xf3;dulo&#xa;6. Package" POSITION="right" ID="ID_1423238433" CREATED="1552590773157" MODIFIED="1552590821557">
<edge COLOR="#ff0000"/>
</node>
<node TEXT="OOP clases" POSITION="left" ID="ID_25434398" CREATED="1546988829776" MODIFIED="1552933117399" LINK="http://localhost:8888/notebooks/Clases-v1.ipynb" HGAP_QUANTITY="73.70731985079694 pt" VSHIFT_QUANTITY="-37.75609931741573 pt">
<edge COLOR="#cc00ff"/>
<node TEXT="classes" ID="ID_352937383" CREATED="1546988829776" MODIFIED="1551463800474" HGAP_QUANTITY="13.121951178664752 pt" VSHIFT_QUANTITY="-9.658537034687741 pt">
<node TEXT="Clases-v3" ID="ID_178743507" CREATED="1552923636225" MODIFIED="1552923652093" LINK="http://localhost:8888/notebooks/Clases-v3.ipynb"/>
</node>
<node TEXT="attributes" ID="ID_966834988" CREATED="1546988829776" MODIFIED="1551463795902" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-7.902439392017243 pt">
<node TEXT="de instancia" ID="ID_306263702" CREATED="1552929410363" MODIFIED="1552929414218"/>
<node TEXT="de clase" ID="ID_1600915557" CREATED="1552929415645" MODIFIED="1552929419347"/>
</node>
<node TEXT="methods" FOLDED="true" ID="ID_583832003" CREATED="1551463782327" MODIFIED="1551463787675">
<node TEXT="@classmethod" ID="ID_890386195" CREATED="1552589201630" MODIFIED="1552590681030"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Sin acceso a atributos de instancia</b>
    </p>
    <p>
      <b>Puede generar otro constructor.</b>
    </p>
    <p>
      
    </p>
    <p>
      &#160;&#160;&#160;&#160;classmethod(function) -&gt; method
    </p>
    <p>
      &#160;&#160;&#160;
    </p>
    <p>
      &#160;&#160;&#160;&#160;Convert a function to be a class method.
    </p>
    <p>
      &#160;&#160;&#160;
    </p>
    <p>
      &#160;&#160;&#160;&#160;A class method receives the class as implicit first argument,
    </p>
    <p>
      &#160;&#160;&#160;&#160;just like an instance method receives the instance.
    </p>
    <p>
      &#160;&#160;&#160;&#160;To declare a class method, use this idiom:
    </p>
    <p>
      &#160;&#160;&#160;
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;class C:
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;@classmethod
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;def f(cls, arg1, arg2, ...):
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;...
    </p>
    <p>
      &#160;&#160;&#160;
    </p>
    <p>
      &#160;&#160;&#160;&#160;It <b>can be called either on the class (e.g. C.f()) or on an instance</b>
    </p>
    <p>
      &#160;&#160;&#160;&#160;(e.g. C().f()).&#160;&#160;The instance is ignored except for its class.
    </p>
    <p>
      &#160;&#160;&#160;&#160;If a class method is called for a derived class, the derived class
    </p>
    <p>
      &#160;&#160;&#160;&#160;object is passed as the implied first argument.
    </p>
    <p>
      &#160;&#160;&#160;
    </p>
    <p>
      &#160;&#160;&#160;&#160;Class methods are different than C++ or Java static methods.
    </p>
    <p>
      &#160;&#160;&#160;&#160;If you want those, see the staticmethod builtin.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="" ID="ID_1916426153" CREATED="1552590037556" MODIFIED="1552590037557">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="classmethod vs staticmethod" ID="ID_1382145641" CREATED="1552589978063" MODIFIED="1552590538125"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      The difference between a static method and a class method is:
    </p>
    <p>
      
    </p>
    <p>
      <b>Static method knows nothing about the class</b>&#160;and&#160;just deals with the parameters
    </p>
    <p>
      <b>Class method works with the class</b>&#160;since its parameter is always the class itself.
    </p>
  </body>
</html>
</richcontent>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="Diferencias" ID="ID_477725542" CREATED="1552590118976" MODIFIED="1552590141116"/>
</node>
<node TEXT="" ID="ID_1686298104" CREATED="1552590037550" MODIFIED="1552590037555">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="dif" ID="ID_1618456267" CREATED="1552590037558" MODIFIED="1552671920953"/>
</node>
</node>
<node TEXT="@staticmethod" ID="ID_796734670" CREATED="1552589217977" MODIFIED="1552590014987" LINK="https://www.programiz.com/python-programming/methods/built-in/staticmethod">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1382145641" STARTINCLINATION="510;0;" ENDINCLINATION="510;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      However, when you need a utility function that doesn't access any properties of a class but makes sense that it belongs to the class, we use static functions.
    </p>
    <p>
      
    </p>
    <p>
      <b>&#160;Having a single implementation</b>
    </p>
    <p>
      Static methods are used when <b>we don't want subclasses of a class change/override a specific implementation of a method</b>.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Herencia" ID="ID_1178473208" CREATED="1552929459404" MODIFIED="1552929466880">
<node TEXT="H. simple" ID="ID_1841337405" CREATED="1552929478349" MODIFIED="1552933325667">
<node TEXT="super()" ID="ID_613564704" CREATED="1552929663506" MODIFIED="1552929672999"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      the method being called by super() needs to exist
    </p>
    <p>
      the caller and callee need to have a matching argument signature
    </p>
    <p>
      and every occurrence of the method needs to use super()
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="overriding" ID="ID_123995364" CREATED="1552933374417" MODIFIED="1552933399353"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      nombre
    </p>
    <p>
      y
    </p>
    <p>
      firma
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="isinstance(v, class)" ID="ID_1421620327" CREATED="1552933421672" MODIFIED="1552933438425"/>
</node>
<node TEXT="H. m&#xfa;ltiple" ID="ID_1385421444" CREATED="1552929468346" MODIFIED="1552933331474">
<node TEXT="orden" ID="ID_1510982603" CREATED="1552929520232" MODIFIED="1552929592684" LINK="https://rhettinger.wordpress.com/2011/05/26/super-considered-super/"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      class LoggingDict(SomeOtherMapping):&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;# new base class
    </p>
    <p>
      &#160;&#160;&#160;&#160;def __setitem__(self, key, value):
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;logging.info('Settingto %r' % (key, value))
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;super().__setitem__(key, value)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;# no change needed
    </p>
    <p>
      In addition to isolating changes, there is another major benefit to computed indirection, one that may not be familiar to people coming from static languages. Since the indirection is computed at runtime, we have the freedom to influence the calculation so that the indirection will point to some other class.
    </p>
    <p>
      
    </p>
    <p>
      The calculation depends on both the class where super is called and on the instance&#8217;s tree of ancestors. The first component, the class where super is called, is determined by the source code for that class. In our example, super() is called in the LoggingDict.__setitem__ method. That component is fixed. The second and more interesting component is variable (we can create new subclasses with a rich tree of ancestors).
    </p>
    <p>
      
    </p>
    <p>
      Let&#8217;s use this to our advantage to construct a logging ordered dictionary without modifying our existing classes:
    </p>
    <p>
      
    </p>
    <p>
      class LoggingOD(LoggingDict, collections.OrderedDict):
    </p>
    <p>
      &#160;&#160;&#160;&#160;pass
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="super() resolution" ID="ID_672578871" CREATED="1552929341714" MODIFIED="1552933254974" LINK="https://rhettinger.wordpress.com/2011/05/26/super-considered-super/"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      class Hereda(Clase1, Clase2):
    </p>
    <p>
      &#160;&#160;&quot;resuelve de izq a der&quot;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="vs composici&#xf3;n" ID="ID_1047748654" CREATED="1552929484318" MODIFIED="1552929496541"/>
</node>
<node TEXT="Polimorfismo" ID="ID_1550000640" CREATED="1552932937512" MODIFIED="1552932943393"/>
<node TEXT="PY2/PY3" ID="ID_1876290177" CREATED="1552925355027" MODIFIED="1552928833088" LINK="http://code.activestate.com/recipes/577721-how-to-use-super-effectively-python-27-version/"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Python 2:</b>
    </p>
    <p>
      
    </p>
    <p>
      class MiSubClase(MiPadreClass):
    </p>
    <p>
      def __init__(self):
    </p>
    <p>
      super(MiSubClase, self).__init__()
    </p>
    <p>
      &#160;
    </p>
    <p>
      <b>Python 3:</b>
    </p>
    <p>
      
    </p>
    <p>
      class MiPadreClase():
    </p>
    <p>
      def __init__(self):
    </p>
    <p>
      pass
    </p>
    <p>
      
    </p>
    <p>
      class SubClase(MiPadreClase):
    </p>
    <p>
      def __init__(self):
    </p>
    <p>
      super()
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Art&#xed;culos" ID="ID_1263263423" CREATED="1552933072408" MODIFIED="1552933102047" LINK="http://www.pythondiario.com/2016/10/herencia-y-polimorfismo-en-python.html"/>
</node>
<node TEXT="FP" POSITION="left" ID="ID_1822402846" CREATED="1546988829777" MODIFIED="1552936117549" LINK="http://localhost:8888/notebooks/Ejemplos%20basicos/Factorial.ipynb" HGAP_QUANTITY="84.24390570681993 pt" VSHIFT_QUANTITY="-36.00000167474521 pt">
<edge COLOR="#ff9900"/>
<node TEXT="map" ID="ID_1274507163" CREATED="1546988829777" MODIFIED="1551463759529" HGAP_QUANTITY="14.0 pt" VSHIFT_QUANTITY="-21.07317171204598 pt"/>
<node TEXT="filter" ID="ID_1807654965" CREATED="1552589802829" MODIFIED="1552589806110"/>
<node TEXT="reduce" ID="ID_1203076641" CREATED="1546988829777" MODIFIED="1551463763671"/>
<node TEXT="lambda" ID="ID_1691367809" CREATED="1552589811394" MODIFIED="1552589815349"/>
<node TEXT="partial/partialmethod" ID="ID_776310535" CREATED="1552936157413" MODIFIED="1552936167608"/>
</node>
</node>
</map>
