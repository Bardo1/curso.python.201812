<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Virtualenv/wrapper" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1545146592259"><hook NAME="MapStyle">
    <properties fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600.0 px" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="2" RULE="ON_BRANCH_CREATION"/>
<node TEXT="virtualenv" POSITION="right" ID="ID_898525850" CREATED="1545146596247" MODIFIED="1545146603892">
<edge COLOR="#ff0000"/>
<node TEXT="install" ID="ID_668585851" CREATED="1546015125036" MODIFIED="1546015128621"/>
<node TEXT="docs" ID="ID_1237102403" CREATED="1546015128968" MODIFIED="1546015130979"/>
<node TEXT="ejemplos" ID="ID_1811779227" CREATED="1546015131653" MODIFIED="1546015135844"/>
</node>
<node TEXT="virtualenvwrapper" POSITION="right" ID="ID_1920334415" CREATED="1545146604384" MODIFIED="1545146608948">
<edge COLOR="#0000ff"/>
<node TEXT="install" ID="ID_1185541788" CREATED="1545146612594" MODIFIED="1545146615932"/>
<node TEXT="docs" ID="ID_174762934" CREATED="1545146616652" MODIFIED="1545146621050"/>
<node TEXT="Ejemplos" ID="ID_1442066888" CREATED="1545146625677" MODIFIED="1545146638943">
<node TEXT="Log" ID="ID_124973348" CREATED="1545146691623" MODIFIED="1545147108936"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      # VirtualenvWrapper :
    </p>
    <p>
      #&#160;&#160;&#160;utilizaci&#243;n de diferentes int&#233;rpretes
    </p>
    <p>
      #&#160;&#160;&#160;y/o librer&#237;as
    </p>
    <p>
      # instaladas a nivel de usuario
    </p>
    <p>
      
    </p>
    <p>
      # cambio de entorno
    </p>
    <p>
      workon -h
    </p>
    <p>
      
    </p>
    <p>
      # intento utilizar el entorno inexistente &quot;p3neoris-curso-e1&quot;
    </p>
    <p>
      workon --p3neoris-curso-e1
    </p>
    <p>
      
    </p>
    <p>
      # creaci&#243;n del entorno inexistente &quot;p3neoris-curso-e1&quot;
    </p>
    <p>
      mkvirtualenv&#160;&#160;p3neoris-curso-e1
    </p>
    <p>
      
    </p>
    <p>
      # creaci&#243;n de otro entorno inexistente &quot;p3neoris-curso-e3&quot;
    </p>
    <p>
      # ! usando PYTHON3
    </p>
    <p>
      mkvirtualenv&#160;&#160;p3neoris-curso-e3 -p python3
    </p>
    <p>
      
    </p>
    <p>
      python --version
    </p>
    <p>
      
    </p>
    <p>
      # conculta de entornos disponibles
    </p>
    <p>
      workon
    </p>
    <p>
      
    </p>
    <p>
      # cambio al entorno python2 del curso
    </p>
    <p>
      workon p3neoris-curso-e1
    </p>
    <p>
      
    </p>
    <p>
      # verificaci&#243;n
    </p>
    <p>
      python --version
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Notebook" ID="ID_892062771" CREATED="1545146700710" MODIFIED="1545146709179">
<node TEXT="Python2" ID="ID_647297367" CREATED="1546014855039" MODIFIED="1546015069886"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      lin-py2-jupyter-botebook.sh
    </p>
    <p>
      
    </p>
    <p>
      ------------------------------------
    </p>
    <p>
      #!/bin/bash -il
    </p>
    <p>
      #!/bin/sh
    </p>
    <p>
      
    </p>
    <p>
      workon PY2
    </p>
    <p>
      cd jupyter
    </p>
    <p>
      jupyter notebook
    </p>
    <p>
      ------------------------------------
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Python3" ID="ID_1645124084" CREATED="1546014865543" MODIFIED="1546015059478"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      lin-py3-jupyter-botebook.sh
    </p>
    <p>
      
    </p>
    <p>
      ------------------------------------
    </p>
    <p>
      #!/bin/bash -il
    </p>
    <p>
      #!/bin/sh
    </p>
    <p>
      
    </p>
    <p>
      workon PY3
    </p>
    <p>
      cd jupyter
    </p>
    <p>
      jupyter notebook
    </p>
    <p>
      ------------------------------------
    </p>
    <p>
      
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Script" ID="ID_500071558" CREATED="1545146725449" MODIFIED="1545146730327"/>
</node>
</node>
</node>
</map>
