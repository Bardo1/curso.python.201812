<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1553808659455"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Python</b>
    </p>
    <p>
      versiones
    </p>
    <p>
      y
    </p>
    <p>
      comparativa
    </p>
  </body>
</html>

</richcontent>
<hook NAME="MapStyle" zoom="1.282">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#808080ff,#808080ff,#808080ff,#808080ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600.0 px" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="6" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Versiones" POSITION="right" ID="ID_1045856236" CREATED="1545056290957" MODIFIED="1551367747500">
<edge COLOR="#ff0000"/>
<node TEXT="2.x (2.7)" ID="ID_1874151200" CREATED="1545056300054" MODIFIED="1545056615349">
<node TEXT="2.4 (old stable)" ID="ID_632175322" CREATED="1546013652947" MODIFIED="1546013718127"/>
<node TEXT="2.6 (some 3.x backported)" ID="ID_1751655806" CREATED="1546013659728" MODIFIED="1546013735146"/>
<node TEXT="2.7 (now)" ID="ID_1933871563" CREATED="1546013677537" MODIFIED="1546013686667"/>
</node>
<node TEXT="3.x (3.3)" ID="ID_251269685" CREATED="1545056330039" MODIFIED="1545056625246"/>
<node TEXT="Elecci&#xf3;n" ID="ID_154744778" CREATED="1545056850313" MODIFIED="1545056858136">
<node TEXT="x Soft/libs existente" ID="ID_1993606600" CREATED="1545061825365" MODIFIED="1551115383782"/>
<node TEXT="x Caracter&#xed;sticas" ID="ID_1203222297" CREATED="1545061883729" MODIFIED="1545061896906"/>
<node TEXT="x Preferencias/conocimiento" ID="ID_208202458" CREATED="1545061861845" MODIFIED="1551115407684"/>
</node>
<node TEXT="historia" ID="ID_1636479615" CREATED="1551449636155" MODIFIED="1551449654189" LINK="https://linuxacademy.com/blog/linux/the-story-of-python-2-and-3/"/>
<node TEXT="seguridad" ID="ID_279122820" CREATED="1551449663552" MODIFIED="1551449675856" LINK="https://snyk.io/blog/python-2-vs-3-security-differences"/>
</node>
<node TEXT="Comparativa 2 vs 3" POSITION="right" ID="ID_921360846" CREATED="1545315959199" MODIFIED="1551449764302" TEXT_SHORTENED="true">
<edge COLOR="#808080"/>
<node TEXT="str : ascii to unicode + bytes" ID="ID_691292909" CREATED="1545061936137" MODIFIED="1551449747998" LINK="https://docs.python.org/3/howto/unicode.html"/>
<node TEXT="new bytes() and b/B" ID="ID_1014363394" CREATED="1545326593785" MODIFIED="1551449834226" LINK="https://docs.python.org/3/howto/unicode.html"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Text Vs. Data Instead Of Unicode Vs. 8-bit
    </p>
    <p>
      Everything you thought you knew about binary data and Unicode has changed.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="print vs print()" ID="ID_990725844" CREATED="1545323598132" MODIFIED="1545323613357"/>
<node TEXT="lists vs {views and iterators}" ID="ID_756876761" CREATED="1545323615380" MODIFIED="1545324049414"/>
<node TEXT="int+long to int" ID="ID_1379969221" CREATED="1545324278606" MODIFIED="1551114772518"/>
<node TEXT="1/2 (int) to (float)" ID="ID_702627013" CREATED="1545324311330" MODIFIED="1551114925689"/>
<node TEXT="sys.maxint vs sys.maxsize" ID="ID_663268905" CREATED="1545324378158" MODIFIED="1545324400673"/>
<node TEXT="repr(long) sin&quot;L&quot;" ID="ID_361322163" CREATED="1545324416579" MODIFIED="1545324431164"/>
<node TEXT="octal 01 vs 0o1&#xa; (cero inicial+letra o]" ID="ID_263323008" CREATED="1545324439381" MODIFIED="1551114973923">
<font BOLD="true"/>
</node>
<node TEXT="bin(1) vs new 0b1 (binary)" ID="ID_166214064" CREATED="1545326540859" MODIFIED="1545326567673"/>
<node TEXT="Text Vs. Data Instead Of Unicode Vs. 8-bit&#xa;Everything you thought you knew about binary data and Unicode has changed." ID="ID_1321155151" CREATED="1545324513565" MODIFIED="1551115020678"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      https://docs.python.org/3/howto/unicode.html
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
<node TEXT="Syntax!!!" ID="ID_1433011675" CREATED="1545325969306" MODIFIED="1545326002425">
<node TEXT="Function argument and return value annotations" ID="ID_1165159295" CREATED="1545326029240" MODIFIED="1545326030688"/>
<node TEXT="Keyword-only arguments" ID="ID_1179894778" CREATED="1545326031289" MODIFIED="1545326233047"/>
<node TEXT="Keyword arguments are allowed after the list of base classes in a class definition." ID="ID_963725298" CREATED="1545326385522" MODIFIED="1545326389499"/>
<node TEXT="nonlocal statement" ID="ID_1278412246" CREATED="1545326391112" MODIFIED="1545326404920"/>
<node TEXT="Extended Iterable Unpacking: a,*b,c=range(5)" ID="ID_938151659" CREATED="1545326405371" MODIFIED="1545326803232"/>
<node TEXT="... otros cambios y remociones ..." ID="ID_1491834666" CREATED="1545326439422" MODIFIED="1545326837396"/>
<node TEXT="3.0 backported to 2.6" ID="ID_1904355752" CREATED="1545326858486" MODIFIED="1545326883844"/>
</node>
<node TEXT="Library changes" ID="ID_1847608092" CREATED="1545326911537" MODIFIED="1545326918796"/>
<node TEXT="String formatting" ID="ID_1640021142" CREATED="1545326935102" MODIFIED="1545326940944">
<node TEXT="" ID="ID_539571651" CREATED="1545327147151" MODIFIED="1545327147151"/>
</node>
<node TEXT="Operators and methods" ID="ID_957682776" CREATED="1545327036807" MODIFIED="1545327051487">
<node TEXT="removed __members__ and __methods__" ID="ID_182840473" CREATED="1545327052395" MODIFIED="1545327074589"/>
</node>
<node TEXT="Builtins" ID="ID_232293812" CREATED="1545327098850" MODIFIED="1545327104295">
<node TEXT="super()" ID="ID_1455306102" CREATED="1545327177309" MODIFIED="1545327185270"/>
<node TEXT="raw_input() removed (only input() )" ID="ID_1838028125" CREATED="1545327185813" MODIFIED="1545327211430"/>
<node TEXT="new next = obj.__next__" ID="ID_1335905944" CREATED="1545327218588" MODIFIED="1545327251425"/>
<node TEXT="round() changes" ID="ID_875659680" CREATED="1545327252505" MODIFIED="1545327274313"/>
<node TEXT="apply(f, *args) to f(*args)" ID="ID_1455605349" CREATED="1545327306062" MODIFIED="1545327378894"/>
<node TEXT="callable() vs isinstance(f, collections.Callable)" ID="ID_1093444080" CREATED="1545327379394" MODIFIED="1545327424992"/>
</node>
<node TEXT="New Class vs Classic Class" ID="ID_1352882312" CREATED="1545327500499" MODIFIED="1551450004174" LINK="https://wiki.python.org/moin/NewClassVsClassicClass" TEXT_SHORTENED="true"/>
<node TEXT="Exception Chaining" ID="ID_1560415933" CREATED="1545336060584" MODIFIED="1551451033014" LINK="https://snyk.io/blog/python-2-vs-3-security-differences"/>
</node>
</node>
</map>
