<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Python&#xa;(plataformas)" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1553809092984"><hook NAME="MapStyle" zoom="1.41">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#808080ff,#808080ff,#808080ff,#808080ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600.0 px" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="5" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Implementado en&#xa;diferentes Plataformas (lang exec)" POSITION="right" ID="ID_894733878" CREATED="1545145219332" MODIFIED="1551364084978">
<edge COLOR="#0000ff"/>
<font BOLD="true"/>
<node TEXT="C" FOLDED="true" ID="ID_1513492502" CREATED="1545145227059" MODIFIED="1545145232969">
<node TEXT="CPython" ID="ID_1224501343" CREATED="1545145257834" MODIFIED="1545145265334"/>
</node>
<node TEXT="Python" FOLDED="true" ID="ID_1424404358" CREATED="1545145234203" MODIFIED="1545145237987">
<node TEXT="PyPy" ID="ID_943558950" CREATED="1545145855978" MODIFIED="1545145868912"/>
</node>
<node TEXT="JVM" FOLDED="true" ID="ID_1270145521" CREATED="1545145238570" MODIFIED="1545145244299">
<node TEXT="Jython" ID="ID_191145307" CREATED="1545245886321" MODIFIED="1549305397439"/>
</node>
<node TEXT=".Net" FOLDED="true" ID="ID_105769235" CREATED="1551364095369" MODIFIED="1551364101208">
<node TEXT="IronPython" ID="ID_1283878158" CREATED="1551364103407" MODIFIED="1551364108996"/>
</node>
</node>
<node TEXT="Implementaciones" POSITION="right" ID="ID_1663075022" CREATED="1545056632608" MODIFIED="1551538406648" LINK="https://www.python.org/download/alternatives/">
<edge COLOR="#0000ff"/>
<node TEXT="CPython" FOLDED="true" ID="ID_1420861154" CREATED="1545056641130" MODIFIED="1545145159192" TEXT_SHORTENED="true">
<node TEXT="Reference implementation" ID="ID_572556754" CREATED="1545066470881" MODIFIED="1545066481518"/>
<node TEXT="C (performant)" ID="ID_257212155" CREATED="1545066484041" MODIFIED="1545066492413"/>
</node>
<node TEXT="Pypy" FOLDED="true" ID="ID_1729198921" CREATED="1547485553651" MODIFIED="1547485722979">
<font SIZE="10"/>
<node TEXT="written in Python" ID="ID_503267317" CREATED="1547485596398" MODIFIED="1547485617616"/>
<node TEXT="RPython (interpreter subset)" ID="ID_1658569214" CREATED="1547485630082" MODIFIED="1547485651637"/>
<node TEXT="JIT" ID="ID_498711101" CREATED="1547485652994" MODIFIED="1547485657332"/>
<node TEXT="~7.5 faster than CPython" ID="ID_323771651" CREATED="1547485658793" MODIFIED="1547485716083"/>
</node>
<node TEXT="PyPi" FOLDED="true" ID="ID_1134984949" CREATED="1545056659002" MODIFIED="1545145172986" TEXT_SHORTENED="true">
<node TEXT="Restricted subset" ID="ID_3458065" CREATED="1545066554892" MODIFIED="1545066592927"/>
<node TEXT="Statically typed" ID="ID_217841495" CREATED="1545066570183" MODIFIED="1545066582060"/>
<node TEXT="CPython compat + speed" ID="ID_630212641" CREATED="1545066907149" MODIFIED="1545066920916"/>
<node TEXT="RPython JIT (C, CLI, JVM backends)" ID="ID_639260639" CREATED="1545066608000" MODIFIED="1545066665151"/>
<node TEXT="Performance up to 5x" ID="ID_992430001" CREATED="1545066668818" MODIFIED="1545066692319"/>
<node TEXT="2.7 stable - 3 beta" ID="ID_1625850659" CREATED="1545066697013" MODIFIED="1545066716059"/>
</node>
<node TEXT="Jython" FOLDED="true" ID="ID_715927587" CREATED="1545057652479" MODIFIED="1545243581174" TEXT_SHORTENED="true">
<node TEXT="(python) to (java) for JVM" ID="ID_592255376" CREATED="1545067033818" MODIFIED="1545067080539"/>
<node TEXT="Can import/use java classes" ID="ID_403018496" CREATED="1545067086717" MODIFIED="1545067114894"/>
<node TEXT="Jython = Python 2.7" ID="ID_145800175" CREATED="1545067116588" MODIFIED="1545067133093"/>
<node TEXT="http://www.jython.org/jythonbook/en/1.0/JythonAndJavaIntegration.html" ID="ID_557733255" CREATED="1549305376469" MODIFIED="1549305380141"/>
</node>
<node TEXT="IronPython" FOLDED="true" ID="ID_1009921452" CREATED="1545065493745" MODIFIED="1545145190026" TEXT_SHORTENED="true">
<node TEXT=".NET implementation" ID="ID_1660688555" CREATED="1545067164711" MODIFIED="1545067197365"/>
<node TEXT="exposes to .NET framework" ID="ID_777191971" CREATED="1545067198177" MODIFIED="1545067210727"/>
<node TEXT="uses .NET libs" ID="ID_14532061" CREATED="1545067224914" MODIFIED="1545067257089"/>
<node TEXT="VS integration" ID="ID_171541172" CREATED="1545067258491" MODIFIED="1545067270067"/>
<node TEXT="IronPython = Python 2.7" ID="ID_1032674873" CREATED="1545067271894" MODIFIED="1545067285353"/>
</node>
<node TEXT="PythonNet" FOLDED="true" ID="ID_896293127" CREATED="1545065517825" MODIFIED="1551117100566" TEXT_SHORTENED="true">
<node TEXT="integrates .NET CLR (Common Language Runtime)" ID="ID_647070994" CREATED="1545067300123" MODIFIED="1545067329549"/>
<node TEXT="complements IronPython" ID="ID_1211213640" CREATED="1545067340386" MODIFIED="1545067358817"/>
<node TEXT="PythonNet + Mono = multiplatform .NET framework" ID="ID_349590095" CREATED="1545067379619" MODIFIED="1545067423486"/>
<node TEXT="Python 2.6 to 3.5" ID="ID_431586967" CREATED="1545067424253" MODIFIED="1545067440063"/>
</node>
<node TEXT="Cython" FOLDED="true" ID="ID_858692421" CREATED="1551116697796" MODIFIED="1551116820883" LINK="https://cython.readthedocs.io/en/latest/src/tutorial/cython_tutorial.html"><richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p http-equiv="content-type" content="text/html; charset=utf-8" style="line-height: 1.5em; color: rgb(62, 67, 73); font-family: Arial, sans-serif; font-size: 14.4px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">
      <font size="1">The fundamental nature of Cython can be summed up as follows: <b>Cython is Python with C data types</b>. </font>
    </p>
    <p style="line-height: 1.5em; color: rgb(62, 67, 73); font-family: Arial, sans-serif; font-size: 14.4px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">
      <font size="1">Cython is Python: Almost any piece of Python code is also valid Cython code. (There are a few&#160;</font><a class="reference internal" href="https://cython.readthedocs.io/en/latest/src/userguide/limitations.html#cython-limitations" style="color: rgb(0, 91, 129); text-decoration: none"><font color="rgb(0, 91, 129)" size="1">Limitations</font></a><font size="1">, but this approximation will serve for now.) The Cython compiler will convert it into C code which makes equivalent calls to the Python/C API. </font>
    </p>
    <p style="line-height: 1.5em; color: rgb(62, 67, 73); font-family: Arial, sans-serif; font-size: 14.4px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">
      <font size="1">But Cython is much more than that, because parameters and variables can be declared to have C data types. Code which manipulates Python values and C values can be freely intermixed, with conversions occurring automatically wherever possible. Reference count maintenance and error checking of Python operations is also automatic, and the full power of Python&#8217;s exception handling facilities, including the try-except and try-finally statements, is available to you &#8211; even in the midst of manipulating C data. </font>
    </p>
  </body>
</html>
</richcontent>
<font SIZE="10"/>
<node TEXT="Extensi&#xf3;n a tipos de C" ID="ID_556126905" CREATED="1551364129159" MODIFIED="1551364145106"/>
<node TEXT="Compila, Importa, ejecuta python compilado en C" ID="ID_842356687" CREATED="1551364146646" MODIFIED="1551364781022"/>
<node TEXT="Importa C libs y ejecuta dentro de c&#xf3;digo Python" ID="ID_1363694250" CREATED="1551365079319" MODIFIED="1551365105094"/>
<node TEXT="Ejecuta ambos: Python bytecode y precompilado en C" ID="ID_1993103172" CREATED="1551364161181" MODIFIED="1551364182950"/>
</node>
<node TEXT="Otras" FOLDED="true" ID="ID_1851956814" CREATED="1545057670310" MODIFIED="1551117087202" LINK="https://wiki.python.org/moin/PythonImplementations">
<node TEXT="CPython variants" ID="ID_1264160307" CREATED="1545145786508" MODIFIED="1551118242759"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <h2 http-equiv="content-type" content="text/html; charset=utf-8" id="CPython_Variants">
      <font size="3">CPython Variants</font>
    </h2>
    <p class="line862">
      These are implementations based on the <a href="https://wiki.python.org/moin/CPython" marked="1">CPython</a>&#160;runtime core (the de-facto reference Python implementation), but with extended behaviour or features in some aspects.
    </p>
    <ul>
      <li>
        <p class="line891">
          <a class="http" href="http://crosstwine.com/linker/python.html" marked="1">CrossTwine Linker</a>&#160;- a combination of CPython and an add-on library offering improved performance (currently proprietary)
        </p>
      </li>
      <li>
        <p class="line891">
          <a href="https://wiki.python.org/moin/StacklessPython" marked="1">Stackless Python</a>&#160;- CPython with an emphasis on concurrency using tasklets and channels (used by <a class="https" href="https://www.develer.com/trac/dspython/" marked="1">dspython</a>&#160;for the Nintendo DS)
        </p>
      </li>
      <li>
        <p class="line891">
          <a class="http" href="http://code.google.com/p/unladen-swallow/" marked="1">unladen-swallow</a>&#160;- &quot;an optimization branch of CPython, intended to be fully compatible and significantly faster&quot;, originally considered for merging with CPython subject to <a class="interwiki" href="http://www.python.org/dev/peps/pep-3146" title="PEP" marked="1">PEP 3146</a>, but now unmaintained
        </p>
      </li>
      <li>
        <p class="line891">
          <a class="http" href="http://code.google.com/p/wpython/" marked="1">wpython</a>&#160;- a re-implementation of CPython using &quot;wordcode&quot; instead of bytecode
        </p>
      </li>
      <li>
        <p class="line891">
          <a class="https" href="https://micropython.org/" marked="1">MicroPython</a>&#160;- Python for micro controllers (runs on the pyboard and the BBC Microbit)
        </p>
      </li>
      <li>
        <p class="line891">
          <a class="http" href="http://www.egenix.com/products/python/PyRun/" marked="1">eGenix PyRun</a>&#160;- Python runtime (CPython + std library) compressed into a single 3-4MB binary
        </p>
      </li>
    </ul>
  </body>
</html>
</richcontent>
<font SIZE="10"/>
</node>
</node>
</node>
</node>
</map>
