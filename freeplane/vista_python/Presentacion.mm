<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Python&#xa;(presentaci&#xf3;n)" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1549049692413"><hook NAME="MapStyle" zoom="1.307">
    <properties fit_to_viewport="false" show_icon_for_attributes="true" show_note_icons="true" edgeColorConfiguration="#808080ff,#808080ff,#808080ff,#808080ff,#808080ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600.0 px" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="12" RULE="ON_BRANCH_CREATION"/>
<font SIZE="11"/>
<node TEXT="Caracter&#xed;sticas" FOLDED="true" POSITION="right" ID="ID_1045856236" CREATED="1545056290957" MODIFIED="1546631232441">
<edge COLOR="#ff0000"/>
<font SIZE="11"/>
<node TEXT="simple - minimalista" ID="ID_1297966089" CREATED="1546015721558" MODIFIED="1552921931469" LINK="http://localhost:8888/notebooks/Ejemplos%20basicos/Minimalista.ipynb"/>
<node TEXT="f&#xe1;cil de aprender" ID="ID_1610618422" CREATED="1546015736396" MODIFIED="1553808328068" LINK="http://localhost:8888/notebooks/Ejemplos%20basicos/Minimalista.ipynb"/>
<node TEXT="alto nivel (abstracci&#xf3;n)" ID="ID_837208824" CREATED="1546015759357" MODIFIED="1552921940746" LINK="http://localhost:8888/notebooks/Ejemplos%20basicos/Factorial.ipynb"/>
<node TEXT="multi paradigma (imp/oop/fp)" ID="ID_1680270096" CREATED="1551116051354" MODIFIED="1552919911396"/>
<node TEXT="portable (muy)" ID="ID_1419725407" CREATED="1546015785912" MODIFIED="1546627280228"/>
<node TEXT="interpretado (bytecode)" ID="ID_1983275168" CREATED="1546015822937" MODIFIED="1546015893685"/>
<node ID="ID_966500124" CREATED="1546015909997" MODIFIED="1551362987562"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Multiprop&#243;sito</b>
    </p>
    <p>
      OOP : todo es un objeto
    </p>
    <p>
      FP : funcional
    </p>
    <p>
      PO : Procedim/Funci&#243;n
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="embebible (en otros leng/scripting)" ID="ID_1944915292" CREATED="1546016083356" MODIFIED="1546016268290"/>
<node TEXT="extensible (mediante otros leng/librer&#xed;as)" ID="ID_920893478" CREATED="1546016205319" MODIFIED="1553808351673"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="2">C: Cython </font>
    </p>
    <p>
      <font size="2">Java: Jython </font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="amplia &quot;standard lib&quot;" ID="ID_1777109334" CREATED="1546016280928" MODIFIED="1546016292492"/>
</node>
<node TEXT="Arquitecturas (hardware)" FOLDED="true" POSITION="right" ID="ID_1450198395" CREATED="1545145905426" MODIFIED="1552920055058">
<edge COLOR="#808080"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      platform module
    </p>
    <p>
      * access underlying platform's data
    </p>
    <p>
      &#160;* hardware
    </p>
    <p>
      &#160;* operating system
    </p>
    <p>
      &#160;* interpreter version information
    </p>
  </body>
</html>
</richcontent>
<node TEXT="x86 32/64 variants" ID="ID_1368623424" CREATED="1545145913978" MODIFIED="1552398440266">
<font BOLD="true"/>
</node>
<node TEXT="ARM, iOS" ID="ID_327148902" CREATED="1545146102495" MODIFIED="1552398440269">
<font BOLD="true"/>
</node>
<node TEXT="PPC" ID="ID_743560347" CREATED="1551363642024" MODIFIED="1551363643936"/>
<node TEXT="...otras... (PPC, Symbian, ...)" ID="ID_127236832" CREATED="1545146109869" MODIFIED="1551450779572"/>
<node TEXT="AIX, IBMi (AS/400, iSeries, System i)" ID="ID_1324032671" CREATED="1551450794933" MODIFIED="1551450824874"/>
<node TEXT="Solaris, OS/390, z/OS, VMS, HP-UX" ID="ID_268473660" CREATED="1551450802521" MODIFIED="1551450836061"/>
<node TEXT="Platform dep: PI/PD" ID="ID_1352061724" CREATED="1545315015918" MODIFIED="1551363949569"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Platform independency</b>: vague term
    </p>
    <p>
      
    </p>
    <p>
      <b>* Platform Independent language:</b>
    </p>
    <p>
      &#160;&#160;Scripts (maybe) and many modules
    </p>
    <p>
      
    </p>
    <p>
      <b>* Platform dependent language:</b>
    </p>
    <p>
      &#160;&#160;Scripts (by design) and platform specific modules
    </p>
    <p>
      
    </p>
    <p>
      <b>Idea of not dependency:</b>
    </p>
    <p>
      
    </p>
    <p>
      <b>&#160;* compileable on any platform with the same source&#160;code:</b>
    </p>
    <p>
      &#160;&#160;&#160;&#160;Almost any language
    </p>
    <p>
      
    </p>
    <p>
      <b>* Copy on a different platform and run immedatly:</b>
    </p>
    <p>
      &#160;&#160;&#160;&#160;Any Scripting Language and Java and .NET for platforms which have the framework (JRE and .NET/mono).
    </p>
  </body>
</html>
</richcontent>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
</node>
<node TEXT="Ejemplos de utilizaci&#xf3;n" FOLDED="true" POSITION="right" ID="ID_35913872" CREATED="1546631079568" MODIFIED="1551107165782">
<edge COLOR="#808080"/>
<font SIZE="11"/>
<node TEXT="Scripting : reemplazo portable de shells bash/ksh/csh/cmd/command)" ID="ID_1992729626" CREATED="1546631090976" MODIFIED="1546631249172">
<font SIZE="10"/>
</node>
<node TEXT="Integration" FOLDED="true" ID="ID_1747505298" CREATED="1546631253646" MODIFIED="1546631321384">
<node TEXT="WLST : Integration: Oracle en Weblogic Scripting Tool" ID="ID_1785560797" CREATED="1546631322908" MODIFIED="1546631351583"/>
</node>
<node TEXT="Tooling" FOLDED="true" ID="ID_442543036" CREATED="1546631495315" MODIFIED="1546631500495">
<node TEXT="ArcGIS/ArcMap" ID="ID_45003729" CREATED="1546631502532" MODIFIED="1546631518545"/>
<node TEXT="ESRI" ID="ID_1828498937" CREATED="1546631528512" MODIFIED="1546631534556"/>
</node>
<node TEXT="Automation" FOLDED="true" ID="ID_550272867" CREATED="1546631639122" MODIFIED="1546631643864">
<node TEXT="Cmd/bash vs python" ID="ID_1975908623" CREATED="1551367266078" MODIFIED="1552576128562"/>
</node>
<node TEXT="Interfacing" FOLDED="true" ID="ID_263389521" CREATED="1546631645142" MODIFIED="1546631649105">
<node TEXT="GSpread: Google SpreadSheets" ID="ID_179528776" CREATED="1546631832765" MODIFIED="1552576144232"/>
</node>
<node TEXT="Google" ID="ID_1511367034" CREATED="1551385447339" MODIFIED="1551385451943"/>
<node TEXT="Librer&#xed;as y apps&#xa;Cient&#xed;ficas" FOLDED="true" ID="ID_205387111" CREATED="1551385576971" MODIFIED="1551385696352">
<font BOLD="true"/>
<node TEXT="Numpy/MatPlotLib/Anaconda" ID="ID_487241174" CREATED="1551385585807" MODIFIED="1551385620145"/>
<node TEXT="Biolog&#xed;a/" ID="ID_477391327" CREATED="1551385601167" MODIFIED="1551385642785"/>
<node TEXT="Radiolog&#xed;a" ID="ID_916117730" CREATED="1552576177840" MODIFIED="1552576184494"/>
<node TEXT="..........." ID="ID_1561737934" CREATED="1552576185901" MODIFIED="1552576188973"/>
</node>
</node>
<node TEXT="Por ejemplo ..." FOLDED="true" POSITION="right" ID="ID_1537584301" CREATED="1551973225207" MODIFIED="1551973235176">
<edge COLOR="#808080"/>
<node TEXT="en Android" FOLDED="true" ID="ID_315524529" CREATED="1551973236870" MODIFIED="1551973248129" LINK="https://wiki.python.org/moin/Android">
<node TEXT="Herramientas" FOLDED="true" ID="ID_1713068148" CREATED="1551973301680" MODIFIED="1551974021097" LINK="https://medium.com/@umerfarooq_26378/tools-to-run-python-on-android-9060663972b4"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      PyDroid 3
    </p>
    <p>
      BeeWare
    </p>
    <p>
      Chaquopy
    </p>
    <p>
      Kivy: packager (app 2/3 a android pkg)
    </p>
    <p>
      Pyqtdeploy
    </p>
    <p>
      QPython ()
    </p>
    <p>
      SL4A
    </p>
    <p>
      PySide
    </p>
    <p>
      Termux
    </p>
    <p>
      ...
    </p>
  </body>
</html>
</richcontent>
<node TEXT="QPython" ID="ID_597289543" CREATED="1551973884851" MODIFIED="1551973925015" LINK="https://dev.to/bauripalash/how-to-run-python-programs-on-android-49bg"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Terminal emulator , Python REPL , Powerful Editor, Many python libraries, Run scripts via QR code , PIP , courses and much more.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="kivy/python-for-android" ID="ID_157358501" CREATED="1551973952659" MODIFIED="1551973968699"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Turn your Python application into an Android APK
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="PyDroid 3" ID="ID_1312874014" CREATED="1551974022998" MODIFIED="1551974046665"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Python IDE for Android
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Kivy" ID="ID_1189394103" CREATED="1551976345876" MODIFIED="1551976355078" LINK="https://kivy.org/doc/stable/gettingstarted/installation.html"/>
</node>
<node TEXT="" ID="ID_1397020546" CREATED="1551973395277" MODIFIED="1551973395277"/>
<node TEXT="" ID="ID_994683493" CREATED="1551973397152" MODIFIED="1551973397152"/>
</node>
<node TEXT="RaspberryPi" FOLDED="true" ID="ID_930784275" CREATED="1552072918556" MODIFIED="1552072926840">
<node TEXT="ARM Linux" ID="ID_1113589047" CREATED="1552072928526" MODIFIED="1552072935183"/>
</node>
</node>
</node>
</map>
