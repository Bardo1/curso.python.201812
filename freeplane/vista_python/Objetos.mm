<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Python b&#xe1;sico&#xa;(objetos)" FOLDED="false" ID="ID_191153586" CREATED="1547495961950" MODIFIED="1554305331960" ICON_SIZE="36.0 pt" LINK="http://localhost:8888/tree/PYTHON_basico" STYLE="oval">
<font SIZE="22" BOLD="true"/>
<hook NAME="MapStyle" zoom="1.1">
    <properties fit_to_viewport="false" show_note_icons="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" backgroundImageURI="../../logo/python-logo-generic.svg"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" ICON_SIZE="64.0 pt" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Segoe Print" SIZE="22"/>
<edge COLOR="#ffffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" ICON_SIZE="32.0 px" COLOR="#000000" BACKGROUND_COLOR="#ffffcc" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="2.6 pt" SHAPE_VERTICAL_MARGIN="2.6 pt" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="18" BOLD="false" ITALIC="true"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" ICON_SIZE="28.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="16"/>
<edge STYLE="bezier" WIDTH="4"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" ICON_SIZE="24.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="14"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" ICON_SIZE="24.0 px" COLOR="#111111" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="13"/>
<edge STYLE="bezier" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" ICON_SIZE="24.0 px" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="12"/>
<edge STYLE="bezier" WIDTH="1"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" ICON_SIZE="24.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" ICON_SIZE="16.0 px">
<font SIZE="10"/>
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="36" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<edge COLOR="#ffffff"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<hook URI="python-logo-generic-11-2.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Objetos" POSITION="right" ID="ID_919167013" CREATED="1551463889841" MODIFIED="1554305307357" LINK="https://docs.python.org/3/library/stdtypes.html">
<edge COLOR="#ff00ff"/>
<node TEXT="Built-ins" ID="ID_550774253" CREATED="1551463906912" MODIFIED="1553809832394" LINK="https://docs.python.org/3.1/library/functions.html"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>numerics, sequences, mappings, classes, instances, exceptions</b>
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Constantes" FOLDED="true" ID="ID_954384166" CREATED="1552331101282" MODIFIED="1552402097461" LINK="https://docs.python.org/3.1/library/constants.html">
<node TEXT="None" ID="ID_166134056" CREATED="1552331179849" MODIFIED="1552331184219"/>
<node TEXT="True" ID="ID_49919600" CREATED="1552331186979" MODIFIED="1552331192208"/>
<node TEXT="False" ID="ID_1741756719" CREATED="1552331192586" MODIFIED="1552331195215"/>
<node TEXT="Ejemplos" ID="ID_1880925608" CREATED="1552594106546" MODIFIED="1552594218545" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Constantes.ipynb"/>
</node>
<node TEXT="Num&#xe9;ricos" FOLDED="true" ID="ID_1670788824" CREATED="1551888440390" MODIFIED="1551888451036">
<node TEXT="PY2: Enteros y Long" ID="ID_1707054989" CREATED="1551888455962" MODIFIED="1551896977688" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Numericos-PY2.ipynb"/>
<node TEXT="PY3: siempre Enteros" ID="ID_1127426454" CREATED="1551896344569" MODIFIED="1551897122528" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Numericos-PY3.ipynb"/>
<node TEXT="Float" ID="ID_1502062634" CREATED="1551888485094" MODIFIED="1552596256895">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1707054989" STARTINCLINATION="169;0;" ENDINCLINATION="169;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="Imaginario" ID="ID_953991489" CREATED="1552330095591" MODIFIED="1552596256893">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1707054989" STARTINCLINATION="139;0;" ENDINCLINATION="139;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="StdLib" FOLDED="true" ID="ID_1365019235" CREATED="1552330108407" MODIFIED="1552596256892" LINK="https://docs.python.org/3.1/library/index.html">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1707054989" STARTINCLINATION="196;0;" ENDINCLINATION="196;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<node TEXT="Decimal" ID="ID_978695672" CREATED="1552329993925" MODIFIED="1552596256891" LINK="https://docs.python.org/3.1/library/decimal.html">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1707054989" STARTINCLINATION="115;0;" ENDINCLINATION="115;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="Complex" ID="ID_757465900" CREATED="1552329259326" MODIFIED="1552596256889" LINK="https://docs.python.org/3.1/library/cmath.html">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1707054989" STARTINCLINATION="144;0;" ENDINCLINATION="144;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="Fractions" ID="ID_1995637959" CREATED="1551898740996" MODIFIED="1552596256888" LINK="https://docs.python.org/3.1/library/fractions.html">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1707054989" STARTINCLINATION="173;0;" ENDINCLINATION="173;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="... y mucho, mucho m&#xe1;s ..." ID="ID_1609290899" CREATED="1552330687180" MODIFIED="1553017155134">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1707054989" STARTINCLINATION="232;0;" ENDINCLINATION="232;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      bin(), oct(), hex(), int(), float(), str()
    </p>
    <p>
      tuple(), list(), dict()
    </p>
    <p>
      set(), frozenset()
    </p>
    <p>
      long() PY2
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="compile() / exec()" ID="ID_635424473" CREATED="1553017222294" MODIFIED="1553017234345"/>
</node>
</node>
<node TEXT="Secuencias" ID="ID_1328541668" CREATED="1551891225225" MODIFIED="1552596487003" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Secuencias-Tuplas-Listas.ipynb">
<node TEXT="PY2 listas vs iterables" ID="ID_1307682106" CREATED="1551900000980" MODIFIED="1551900017408"/>
<node TEXT="PY3 iterables" ID="ID_191926738" CREATED="1551900018612" MODIFIED="1551900024459"/>
<node TEXT="Tuplas" ID="ID_932629046" CREATED="1552596492889" MODIFIED="1553873708403" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Secuencias-Tuplas-Listas.ipynb"/>
<node TEXT="Sets / Dicts" ID="ID_1700846686" CREATED="1551464140354" MODIFIED="1551897214827" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Objetos-Set-Dict.ipynb"/>
<node TEXT="frozenset([iterable])" ID="ID_1039919805" CREATED="1553016939415" MODIFIED="1553016945652"/>
</node>
<node TEXT="Mapeos" ID="ID_1331242577" CREATED="1552676279169" MODIFIED="1552676285237"/>
<node TEXT="Clases" ID="ID_755347412" CREATED="1552676288200" MODIFIED="1552676291469"/>
<node TEXT="Instancias" ID="ID_1228166" CREATED="1552676293140" MODIFIED="1552676297509"/>
<node TEXT="Excepciones" ID="ID_1482379177" CREATED="1551891229848" MODIFIED="1552405816405" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Tipos-base-Excepciones-Py3.ipynb"/>
<node TEXT="Built-in methods list" ID="ID_320375326" CREATED="1554484624331" MODIFIED="1554484693923"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      BUILT-IN METHODS
    </p>
    <p>
      abs()
    </p>
    <p>
      any()
    </p>
    <p>
      all()
    </p>
    <p>
      ascii()
    </p>
    <p>
      bin()
    </p>
    <p>
      bool()
    </p>
    <p>
      bytearray()
    </p>
    <p>
      callable()
    </p>
    <p>
      bytes()
    </p>
    <p>
      chr()
    </p>
    <p>
      compile()
    </p>
    <p>
      classmethod()
    </p>
    <p>
      complex()
    </p>
    <p>
      delattr()
    </p>
    <p>
      dict()
    </p>
    <p>
      dir()
    </p>
    <p>
      divmod()
    </p>
    <p>
      enumerate()
    </p>
    <p>
      staticmethod()
    </p>
    <p>
      filter()
    </p>
    <p>
      eval()
    </p>
    <p>
      float()
    </p>
    <p>
      format()
    </p>
    <p>
      frozenset()
    </p>
    <p>
      getattr()
    </p>
    <p>
      globals()
    </p>
    <p>
      exec()
    </p>
    <p>
      hasattr()
    </p>
    <p>
      help()
    </p>
    <p>
      hex()
    </p>
    <p>
      hash()
    </p>
    <p>
      input()
    </p>
    <p>
      id()
    </p>
    <p>
      isinstance()
    </p>
    <p>
      int()
    </p>
    <p>
      issubclass()
    </p>
    <p>
      iter()
    </p>
    <p>
      list() Function
    </p>
    <p>
      locals()
    </p>
    <p>
      len()
    </p>
    <p>
      max()
    </p>
    <p>
      min()
    </p>
    <p>
      map()
    </p>
    <p>
      next()
    </p>
    <p>
      memoryview()
    </p>
    <p>
      object()
    </p>
    <p>
      oct()
    </p>
    <p>
      ord()
    </p>
    <p>
      open()
    </p>
    <p>
      pow()
    </p>
    <p>
      print()
    </p>
    <p>
      property()
    </p>
    <p>
      range()
    </p>
    <p>
      repr()
    </p>
    <p>
      reversed()
    </p>
    <p>
      round()
    </p>
    <p>
      set()
    </p>
    <p>
      setattr()
    </p>
    <p>
      slice()
    </p>
    <p>
      sorted()
    </p>
    <p>
      str()
    </p>
    <p>
      sum()
    </p>
    <p>
      tuple() Function
    </p>
    <p>
      type()
    </p>
    <p>
      vars()
    </p>
    <p>
      zip()
    </p>
    <p>
      __import__()
    </p>
    <p>
      super()
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="V/F" FOLDED="true" ID="ID_928699823" CREATED="1552328636249" MODIFIED="1552412835067" LINK="http://localhost:8888/notebooks/PYTHON_basico/004-Verdadero-Falso.ipynb">
<node TEXT="Falso" FOLDED="true" ID="ID_478712019" CREATED="1552328636249" MODIFIED="1552331455239">
<node TEXT="None" ID="ID_488296213" CREATED="1552331217526" MODIFIED="1552331221171"/>
<node TEXT="False" ID="ID_1514116473" CREATED="1552331221697" MODIFIED="1552331224199"/>
<node TEXT="Cero de todo tipo" ID="ID_1924676671" CREATED="1552331224988" MODIFIED="1552331305605"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      0 0.0 0j
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Secuencias vac&#xed;as" ID="ID_34133222" CREATED="1552331241860" MODIFIED="1552331322429"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      ''&#160;&#160;&quot;&quot;&#160;&#160;()&#160;&#160;[]
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Mapeos vac&#xed;os" ID="ID_1436041894" CREATED="1552331253014" MODIFIED="1552331341100"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      {}
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Instancias de clases de usuario" ID="ID_940623780" CREATED="1552331362077" MODIFIED="1552331409843"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      __bool__() == 0
    </p>
    <p>
      __len__() == 0
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Verdadero" FOLDED="true" ID="ID_217890001" CREATED="1552331456447" MODIFIED="1552331463976">
<node TEXT="si no es FALSO!!" ID="ID_150031882" CREATED="1552331465939" MODIFIED="1552596808383" LINK="http://localhost:8888/notebooks/00-01-Idioms-Truthy-Falsy.ipynb"/>
</node>
<node TEXT="Comparaciones" ID="ID_1743863901" CREATED="1552419460093" MODIFIED="1552419465804"/>
</node>
<node TEXT="print y representaci&#xf3;n" FOLDED="true" ID="ID_1882711244" CREATED="1551464553965" MODIFIED="1551464569120">
<node TEXT="concatenando" FOLDED="true" ID="ID_1610398594" CREATED="1551891358769" MODIFIED="1552592809580" LINK="http:/localhost:8888/notebooks/PYTHON_basico/005-print-format.ipynb">
<node TEXT="strings" ID="ID_1787494782" CREATED="1552416065416" MODIFIED="1553809869384"/>
<node TEXT="format tipo C" ID="ID_1997193408" CREATED="1551891371902" MODIFIED="1553809862620"/>
<node TEXT="format &quot;moderno&quot;" ID="ID_336542986" CREATED="1551891392474" MODIFIED="1553809858512"/>
</node>
<node ID="ID_883745084" CREATED="1552415889047" MODIFIED="1553809887169" LINK="https://pyformat.info/"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Comparativa
    </p>
    <p>
      <b>% vs .format</b>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="str() vs repr()" FOLDED="true" ID="ID_14941352" CREATED="1552417856909" MODIFIED="1552417865437">
<node ID="ID_1549979504" CREATED="1552417906957" MODIFIED="1552418917351"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>repr: eval(repr) : valid</b>
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &gt;&gt;&gt;repr(y)
    </p>
    <p>
      &quot;'a string'&quot;
    </p>
    <p>
      &gt;&gt;&gt;y2=eval(repr(y))
    </p>
    <p>
      &gt;&gt;&gt;y==y2
    </p>
    <p>
      True
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="__str__ goal is to be readable" ID="ID_811231769" CREATED="1552417928421" MODIFIED="1552417937550"/>
</node>
</node>
<node TEXT="Idioms" FOLDED="true" ID="ID_1930758380" CREATED="1551463923406" MODIFIED="1553809978397" LINK="http://localhost:8888/tree"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Art&#237;culos:</b>
    </p>
    <ul>
      <li>
        <a href="https://recursospython.com/guias-y-manuales/booleanos-operaciones-logicas-y-binarias/"><b>booleanos-operaciones-logicas-y-binarias </b></a>
      </li>
      <li>
        <a href="https://hackernoon.com/going-beyond-the-idiomatic-python-a321b6c6a5e6"><b>hackernoon.com going-beyond-the-idiomatic-python </b></a>
      </li>
    </ul>
  </body>
</html>
</richcontent>
<node TEXT="variables y&#xa;asignaci&#xf3;n" ID="ID_1582955275" CREATED="1553002842099" MODIFIED="1553006474923" LINK="http://localhost:8888/notebooks/00-01-Idioms-Variables.ipynb"/>
<node TEXT="condicionales" ID="ID_1252251465" CREATED="1552416163060" MODIFIED="1553809866686" LINK="http://localhost:8888/notebooks/00-01-Idioms-Condicionales.ipynb"/>
<node TEXT="loops" ID="ID_640600696" CREATED="1552416169681" MODIFIED="1553809890737" LINK="http://localhost:8888/notebooks/PYTHON_basico/006-Loops.ipynb"/>
<node TEXT="iterators" ID="ID_1497607550" CREATED="1552420559695" MODIFIED="1553006969503" LINK="http://localhost:8888/notebooks/00-REPL/itertools_py2-detallado.ipynb"/>
<node TEXT="generators" ID="ID_854111921" CREATED="1552420554029" MODIFIED="1554425519392" LINK="http://localhost:8888/notebooks/Ejemplos%20basicos/Generadores%20vs%20Listas.ipynb"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <a href="https://medium.com/@jasonrigden/a-quick-guide-to-python-generators-and-yield-statements-89a4162c0ef8">a-quick-guide-to-python-generators-and-yield-statements </a>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="comprehensions" ID="ID_478502313" CREATED="1553007433077" MODIFIED="1553007439597"/>
<node TEXT="ayudas" FOLDED="true" ID="ID_607580108" CREATED="1552424597058" MODIFIED="1553014248886"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Ayudas:
    </p>
    <p>
      * enumerate
    </p>
    <p>
      * zip
    </p>
  </body>
</html>
</richcontent>
<node TEXT="enumerate()" ID="ID_617107152" CREATED="1553014742518" MODIFIED="1553016300671" LINK="http://localhost:8888/notebooks/00-01-Idioms-Ayudas-Enumerate.ipynb"/>
<node TEXT="zip()" ID="ID_1141211629" CREATED="1553014753561" MODIFIED="1553016295164" LINK="http://localhost:8888/notebooks/00-01-Idioms-Ayudas-Zip.ipynb"/>
<node TEXT="any()&#xa;all()" ID="ID_1516833629" CREATED="1553016279207" MODIFIED="1553016802534" LINK="http://localhost:8888/notebooks/00-01-Idioms-Ayudas-All-Any.ipynb"/>
<node TEXT="compile()" ID="ID_1548965047" CREATED="1553016883372" MODIFIED="1553016891275"/>
<node TEXT="sorted()" ID="ID_985269059" CREATED="1553658554539" MODIFIED="1553658590536"/>
<node TEXT="reversed()" ID="ID_480884669" CREATED="1554433199680" MODIFIED="1554433205781"/>
</node>
</node>
</node>
</node>
</map>
