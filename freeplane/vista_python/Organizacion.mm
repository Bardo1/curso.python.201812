<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Organizaci&#xf3;n" FOLDED="false" ID="ID_191153586" CREATED="1546988829764" MODIFIED="1553001985882" ICON_SIZE="36.0 pt" STYLE="oval">
<font SIZE="22"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" ICON_SIZE="64.0 pt" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Segoe Print" SIZE="22"/>
<edge COLOR="#ffffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" ICON_SIZE="32.0 px" COLOR="#000000" BACKGROUND_COLOR="#ffffcc" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="2.6 pt" SHAPE_VERTICAL_MARGIN="2.6 pt" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="18" BOLD="false" ITALIC="true"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" ICON_SIZE="28.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="16"/>
<edge STYLE="bezier" WIDTH="4"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" ICON_SIZE="24.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="14"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" ICON_SIZE="24.0 px" COLOR="#111111" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="13"/>
<edge STYLE="bezier" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" ICON_SIZE="24.0 px" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="12"/>
<edge STYLE="bezier" WIDTH="1"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" ICON_SIZE="24.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" ICON_SIZE="16.0 px">
<font SIZE="10"/>
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="27" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<edge COLOR="#ffffff"/>
<hook URI="python-logo-generic-11-2.png" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>1. Sentencia </b>
    </p>
    <p>
      <b>2. L&#237;nea de comando </b>
    </p>
    <p>
      <b>3. M&#243;dulo (Archivo fuente) </b>
    </p>
    <p>
      <b>4. Librer&#237;a </b>
    </p>
    <p>
      <b>6. Package</b>
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Codificando..." POSITION="right" ID="ID_425552906" CREATED="1552999493132" MODIFIED="1553000308566">
<edge COLOR="#00ffff"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="inmediato" ID="ID_575079876" CREATED="1546988829777" MODIFIED="1553000541287" HGAP_QUANTITY="67.56097810145019 pt" VSHIFT_QUANTITY="-42.14634342409195 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>python -c &quot;import sys; print sys.version&quot;</b>
    </p>
  </body>
</html>
</richcontent>
<font SIZE="18"/>
</node>
<node TEXT="script" ID="ID_1968973655" CREATED="1547469042746" MODIFIED="1553000616079"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>echo &quot;import sys; print sys.path&quot; &gt; script.py</b>
    </p>
    <p>
      <b>python script.py</b>
    </p>
  </body>
</html>
</richcontent>
<font BOLD="false"/>
<node TEXT="file.ext" ID="ID_273436263" CREATED="1546988829774" MODIFIED="1549295287002" HGAP_QUANTITY="91.26829627750192 pt" VSHIFT_QUANTITY="-30.731708746733723 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b><font size="4">Usuales</font></b>
    </p>
    <p>
      <font size="4">.py&#160;&#160;&#160;&#160;&#160;source code </font>
    </p>
    <p>
      <font size="4">.pyc&#160;&#160;&#160;&#160;compiled bytecode </font>
    </p>
    <p>
      <font size="4">.pyo&#160;&#160;&#160;&#160;pre Python 3.5 : bytecode &quot;optimized&quot; (-O) </font>
    </p>
    <p>
      <font size="4">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;- assert </font>
    </p>
    <p>
      <font size="4">.pyd&#160;&#160;&#160;&#160;basically a windows dll file</font>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="m&#xf3;dulo" ID="ID_900075296" CREATED="1547469083496" MODIFIED="1552999537320" LINK="Modules_define.mm">
<node TEXT="import from MODULE" ID="ID_495676614" CREATED="1546988829775" MODIFIED="1549296340841" HGAP_QUANTITY="101.80488213352491 pt" VSHIFT_QUANTITY="-42.14634342409195 pt">
<node TEXT="M&#xf3;dulo simple" ID="ID_744391648" CREATED="1546988829775" MODIFIED="1549295918230" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-14.926829962699236 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4"># any file with code is a module </font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4">import os </font>
    </p>
    <p>
      <font size="4">print os.sep</font>
    </p>
  </body>
</html>
</richcontent>
<font SIZE="14"/>
</node>
<node TEXT="SubM&#xf3;dulos" ID="ID_903669612" CREATED="1546988829775" MODIFIED="1549296414604"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">Jerarqu&#237;a de carpetas </font>
    </p>
    <p>
      <b><font size="4">sin __init__.py</font></b>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Namespacing" ID="ID_1561680840" CREATED="1549295255898" MODIFIED="1549295268873"/>
<node TEXT="Uso como MODULO o SCRIPT" ID="ID_332717920" CREATED="1549297196218" MODIFIED="1549300046689"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">#!/usr/bin/env python3 </font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4"># ejemplos/modulos_001.py </font>
    </p>
    <p>
      <font size="4">import os </font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4">def mi_path(): </font>
    </p>
    <p>
      <font size="4">&#160;&#160;nvo_path=&quot;aa&quot;+os.sep+&quot;bb&quot; </font>
    </p>
    <p>
      <font size="4">&#160;&#160;return nvo_path </font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4">if __name__=&quot;__main__&quot;: </font>
    </p>
    <p>
      <font size="4">&#160;&#160;print mi_path() </font>
    </p>
    <p>
      
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="packages" ID="ID_942167910" CREATED="1549295218913" MODIFIED="1552999541641">
<node TEXT="Jerarqu&#xed;a de carpetas&#xa;+ m&#xf3;dulos&#xa;+ __init__.py" ID="ID_1501478608" CREATED="1549295926993" MODIFIED="1549296006103"/>
<node TEXT="Carpetas =&gt; visibilidad (namespacing)" ID="ID_459145747" CREATED="1549296015034" MODIFIED="1549296038141"/>
<node TEXT="Archivos.py =&gt; M&#xf3;dulos componentes del package" ID="ID_1632146113" CREATED="1549296040385" MODIFIED="1549296064511"/>
<node TEXT="Jerarqu&#xed;a:&#xa;import my-pack&#xa;import my-pack.my_mod1&#xa;import my-pack.my_mod1.mod1a&#xa;import my-pack.my_mod1.mod1b&#xa;import my-pack.my_mod2.modda&#xa;import my-pack.my_mod2.mod2b" ID="ID_657451311" CREATED="1549296066038" MODIFIED="1549296170127"/>
<node TEXT="__init__py:&#xa;version&#xa;imports" ID="ID_572746626" CREATED="1549296173966" MODIFIED="1549296241882"/>
</node>
<node TEXT="librer&#xed;a" ID="ID_90823870" CREATED="1546988829775" MODIFIED="1552999551940" HGAP_QUANTITY="56.146343424091945 pt" VSHIFT_QUANTITY="-61.46341749346744 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">conjunto de m&#243;dulos/packages</font>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Packages vs modules" POSITION="right" ID="ID_1121819438" CREATED="1549296462383" MODIFIED="1555069474074">
<edge COLOR="#7c7c00"/>
<richcontent TYPE="DETAILS" HIDDEN="true">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Any module that contains a __path__ attribute is considered a package</b>.
    </p>
    <p>
      import xx
    </p>
    <p>
      &#160;de module o package es SIEMPRE
    </p>
    <p>
      type(xx) == module
    </p>
    <p>
      
    </p>
    <p>
      Package import
    </p>
    <p>
      Visibles seg&#250;n __init__.py
    </p>
    <p>
      
    </p>
    <p>
      Ejemplo: # https://stackoverflow.com/questions/7948494/whats-the-difference-between-a-python-module-and-a-python-package
    </p>
    <p>
      
    </p>
    <p>
      $ mkdir -p a/b
    </p>
    <p>
      $ touch a/b/c.py
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      import a
    </p>
    <p>
      a&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#8658; &lt;module 'a' (namespace)&gt;
    </p>
    <p>
      a.b&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#8658; AttributeError: module 'a' has no attribute 'b'
    </p>
    <p>
      import a.b.c
    </p>
    <p>
      a.b&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#8658; &lt;module 'a.b' (namespace)&gt;
    </p>
    <p>
      a.b.c&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#8658; &lt;module 'a.b.c' from '/home/cjs/a/b/c.py'&gt;
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
    <p>
      
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</map>
