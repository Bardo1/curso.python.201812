<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Ejemplos" FOLDED="false" ID="ID_191153586" CREATED="1551276150495" MODIFIED="1552399728989" ICON_SIZE="36.0 pt" LINK="http://localhost:8888/tree/PYTHON_basico" STYLE="oval">
<font SIZE="22"/>
<hook NAME="MapStyle" zoom="1.001">
    <properties show_icon_for_attributes="true" fit_to_viewport="false" show_note_icons="true" show_notes_in_map="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" backgroundImageURI="../../logo/python-logo-master-v3-TM-flattened.png"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" ICON_SIZE="64.0 pt" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Segoe Print" SIZE="22"/>
<edge COLOR="#ffffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" ICON_SIZE="32.0 px" COLOR="#000000" BACKGROUND_COLOR="#ffffcc" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="2.6 pt" SHAPE_VERTICAL_MARGIN="2.6 pt" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="18" BOLD="false" ITALIC="true"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" ICON_SIZE="28.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="16"/>
<edge STYLE="bezier" WIDTH="4"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" ICON_SIZE="24.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="14"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" ICON_SIZE="24.0 px" COLOR="#111111" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="13"/>
<edge STYLE="bezier" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" ICON_SIZE="24.0 px" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="12"/>
<edge STYLE="bezier" WIDTH="1"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" ICON_SIZE="24.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" ICON_SIZE="16.0 px">
<font SIZE="10"/>
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="35" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<edge COLOR="#ffffff"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="Considerar" POSITION="right" ID="ID_1231333258" CREATED="1552669457268" MODIFIED="1552914743287">
<edge COLOR="#0000ff"/>
<node TEXT="compatibilidad" ID="ID_937236858" CREATED="1552399746027" MODIFIED="1552669491752">
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="codificaci&#xf3;n" ID="ID_1088077280" CREATED="1552580859551" MODIFIED="1552663668170" LINK="http://localhost:8888/notebooks/PYTHON_basico/002%20Codificaci%C3%B3n.ipynb"/>
<node TEXT="print" ID="ID_153079906" CREATED="1552666754894" MODIFIED="1552666760059">
<node TEXT="sentencia PY2" ID="ID_92143619" CREATED="1552666773889" MODIFIED="1552666836663" LINK="http://localhost:8888/notebooks/compatibilizar_Py2.ipynb"/>
<node TEXT="funci&#xf3;n PY3" ID="ID_1805870810" CREATED="1552666791407" MODIFIED="1552666858434" LINK="http://localhost:8888/notebooks/compatibilizar_Py3.ipynb"/>
<node TEXT="from __future__ import print_function" ID="ID_225326906" CREATED="1552666801630" MODIFIED="1552666847904" LINK="http://localhost:8888/notebooks/compatibilizar_Py2_future.ipynb"/>
<node TEXT="import six" ID="ID_1195047396" CREATED="1552666818366" MODIFIED="1552666871047" LINK="http://localhost:8888/notebooks/compatibilizar_Six_Py2_Py3.ipynb"/>
</node>
<node TEXT="divisi&#xf3;n" ID="ID_1080688539" CREATED="1552586763328" MODIFIED="1552588532199" LINK="http://localhost:8888/notebooks/PYTHON_basico/002%20Division.ipynb#"/>
<node TEXT="Compatibilidad&#xa; en proyectos" ID="ID_999658729" CREATED="1552915638537" MODIFIED="1552915659630">
<node TEXT="Ejemplo" ID="ID_1667759102" CREATED="1552915661936" MODIFIED="1552915691365" LINK="https://github.com/web2py/pydal/wiki/Write-py2py3-compatible-code"/>
</node>
</node>
<node TEXT="Tipos base" ID="ID_825073479" CREATED="1552400612792" MODIFIED="1552669495974" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Tipos-base-PY3.ipynb">
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="num&#xe9;ricos PY2" ID="ID_1080526883" CREATED="1552667016584" MODIFIED="1552668439215" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Numericos-PY2.ipynb"/>
<node TEXT="num&#xe9;ricos PY3" ID="ID_943429105" CREATED="1552668445152" MODIFIED="1552668468547" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Numericos-PY3.ipynb"/>
</node>
</node>
<node TEXT="cadenas" POSITION="right" ID="ID_1304074997" CREATED="1552399684814" MODIFIED="1552664585650">
<edge COLOR="#007c00"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
</node>
<node TEXT="print formateado" POSITION="right" ID="ID_175789810" CREATED="1552397382512" MODIFIED="1552664585651">
<edge COLOR="#00007c"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
</node>
<node TEXT="C&#xf3;digo" POSITION="left" ID="ID_177146414" CREATED="1551991349430" MODIFIED="1555069646059" LINK="Ejemplos%20avanzados.mm">
<edge COLOR="#00ffff"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
</node>
</node>
</map>
