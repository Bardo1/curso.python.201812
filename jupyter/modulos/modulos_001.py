#!/usr/bin/env python3

# ejemplos/modulos_001.py
import os

print ("... MODULOS_001")

def mi_path():
  print ("... MODULOS_001.mi_path")
  nvo_path="aa"+os.sep+"bb"
  return nvo_path


if __name__=="__main__":
  print ("... MODULOS_001 MAIN")
  print (mi_path())

