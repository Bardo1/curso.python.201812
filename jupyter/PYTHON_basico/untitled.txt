# Tips iniciales
   * Ayuda en línea:
   > * help(int)
    
    
# Librerías:
    * pip install  PACKAGE-NAME
    * pip install  -r requirements.txt
    * pip install --download CARPETA -r requirements.txt
    
    
# Ejemplos

    Factorial
    Fibonacci
    Data Analisis (simple)
    
## Paso a paso
    Imperativo
        > if/for
    
    Modulado
        > Función simple
            * documentada
            * con doctests
            * con __main__
            