#!/usr/bin/env python2
# -*- coding: utf-8 -*- unix
# -*- coding: cp-1252 -*- windows
# !!! informa al intérprete
# que puede encontrar esta codificación NO ASCII !!!

from __future__ import print_function

print("aeio")

print("áéíóú")


# also a comment in Python?

# Command line : ejecuta y presenta resultado (si corresponde)

# Puedo imprimir
#print "me/pycharm/code running tests"
print ( "me/pycharm/code running tests")

# O sólo escribir lo que quiero evaluar
11

# Pedirle su tipo (clase)
"tipo = clase"
(11.1).__class__
type(11.1)

# Los presento juntos (genera una TUPLA)
11 , 1.1 , 1e1


# Presentación correcta de los tipos
print( "None:", None, (None).__class__ )

print( "LOGICO:", True, (True).__class__ )
print( "LOGICO:", False, (False).__class__ )

print( "ENTERO:", 1, (1).__class__ )

print( "FLOAT:", 1.1 , (11.1).__class__ )
print( "FLOAT:", float(1), float(1).__class__ )
print( "FLOAT:", float("1"), float("11").__class__ )
print( "FLOAT:", 1.2e3, (1.2e3).__class__ )

print( "IMAGINARIO:" , 3J, 3j , (3J).__class__, (3j).__class__ )

print( "STRING:", "a", ("a").__class__ )

print( "TUPLA: ", (12, 3.2, "asd", 3+4j) )
print( "ARRAY: ", [12, 3.2, "asd", 3+4j] )
print( "SET:   ", {12, 3.2, "asd", 3+4j} )
print( "DICT:  ", {"d": 12, "ed": 3.2, "pre": "asd", 55: 3+4j} )
print( "DICT:  ", {33: 12, "ed": 3.2, "pre": "asd", 55: 3+4j} )

# Obvio: puedo asignar y consultar por el contenido
a=1
print( "a: ", a, a.__class__, type(a) )

