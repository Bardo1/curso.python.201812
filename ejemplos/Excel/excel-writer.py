

"""Ejemplo de escritura de archivo Excel (formato excel)
"""
import xlsxwriter


filename = "./salida.xlsx"

wb = xlsxwriter.Workbook(filename)
wb_salida = wb.add_worksheet("Salida")

for i in range(1, 10):
    try:
        # wb_salida.write( "a%s"%i, "Celda a-%s1"%i)
        wb_salida.write( 0, i, "Celda a-%s1"%i)
    except Exception as exc:
        print("EXCEPCION:", exc)

else:
    print("Ok: ",i)

wb.close()
