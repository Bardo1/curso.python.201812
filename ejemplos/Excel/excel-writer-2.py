

"""Ejemplo de escritura de archivo Excel (formato excel)
"""
import xlsxwriter


filename = "./salida.xlsx"

wb = xlsxwriter.Workbook(filename)
wb_salida = wb.add_worksheet("Resultados")

a_impactar = (
    (1, 3, "Treinta y uno"),
    (5, 1, "quince y medio"),
    (12, 21, """lejos,
    lejos,
    lejos""")
)

for fila, col, valor in a_impactar:
    try:
        wb_salida.write( fila, col, valor)
    except Exception as exc:
        print("EXCEPCION:", exc)

else:
    print("Ok: ")

wb.close()
