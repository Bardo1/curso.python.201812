# *-* coding: utf8 *-*
import unittest

import sys
sys.path.append('.')
sys.path.append('..')
# print ("sys.path ")
# print (sys.path )

try:

        print("from promediar import promedio")
        from promediar import promedio
        print("from promediar import suma")
        from promediar import suma

        # print("from .promediar import promedio")
        # from .promediar import promedio
        # print("from .promediar import suma")
        # from .promediar import suma
        # # from .promediar import promedio
except Exception as exc:
    print("*** ERROR al importar la función ***")

print("... a probar ...")

class TestFuncionesEstadisticas(unittest.TestCase):
    """Tests de la funcio promedio, en módulo aparte"""
    def test_promedio(self):
        self.assertEqual(promedio([20, 30, 70]), 40.0)

        self.assertEqual(round(promedio([1, 5, 7]), 1), 4.3)

        # !!! ERROR !!!
        self.assertEqual( promedio([1, ]), 1.1)


        with self.assertRaises(ZeroDivisionError):
            promedio([])

        with self.assertRaises(TypeError):
            promedio(20, 30, 70)

    def test_suma(self):
        self.assertEqual(suma(1,2), 1.5)

unittest.main()
# llamarlo de la linea de comandos ejecuta todas las pruebas
