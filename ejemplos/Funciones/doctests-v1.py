#
# def promedio_v0(valor1, valor2, valor3=0):
#
#     return (valor1+valor2+valor3)/3

def promedio(valores):

    """Calcula la media aritmética de una secuencia de números.

    >>> print(promedio([20, 30, 70]))
    40.0

    >>> print(promedio([1]))
    1.0

    >>> print(promedio([1,2]))
    1.1

    """
    return sum(valores) / len(valores)

def suma(valores):
    '''
        Suma la lista de "valores"

    >>> suma((1,2))
    3
    '''
    #TODO: implementar!!
    return  sum(valores)


if __name__=='__main__':
    # print(suma( (1, 2, 3) ))
    # print(suma( [1, 2, 3] ))
    #
    # print("MAIN")
    import doctest

    doctest.testmod()
    # # valida automáticamente las pruebas integradas

    doctest_ = "no coincide con el nombre de libreria"
