#!/usr/bin/env jython

def java_from_python():
    """

    :return:
# http://www.jython.org/jythonbook/en/1.0/JythonAndJavaIntegration.html

# from java.lang import Math
# Math.max(4, 7)
# 7L
# Math.pow(10,5)
# 100000.0
# Math.round(8.75)
# 9L
# Math.abs(9.765)
# 9.765
# Math.abs(-9.765)
# 9.765
# from java.lang import System as javasystem
# javasystem.out.println("Hello")
# Hello
"""

from java.lang import Math

print Math.max(4, 7)

assert(Math.max(4, 7) == 7L)
print Math.pow(10,5)


assert(Math.pow(10,5) == 100000.0)
print Math.round(8.75)


assert(Math.round(8.75) == 9L)
print Math.abs(9.765)

assert(Math.abs(9.765) == 9.765)
print Math.abs(-9.765)

assert(Math.abs(-9.765) == 9.765)
from java.lang import System as javasystem

javasystem.out.println("Hello")

# Hello
