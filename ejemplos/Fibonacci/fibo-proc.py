
def fibo(n):
    """Cálculo de la sucesión de Fibonacci

        a: suma previa
        b: nueva suma

    >>> fibo(0)
    0
    >>> fibo(1)
    1
    >>> fibo(2)
    1
    >>> fibo(3)
    2
    >>> fibo(4)
    3
    >>> fibo(5)
    5
    >>> fibo(10)
    55
    """

    if n < 2:
        b = n
    else:
        a, b = 0, 1
        for i in range(2, n + 1):
            a, b = b, a + b

    return b

if __name__=="__main__":
    a_verificar = 10
    for i in range(a_verificar):
        print("%6s : %6s" % (i, fibo(i)))


    import doctest
    doctest.testmod()
