#!/usr/bin/env python3

# ejemplos/modulos_001.py
import os

print ("... MODULOS_001")

CONSTANTES = {"A":"a", "B":"b", "PATH":"mi_path2"}

def mi_path():
  print ("... MODULOS_001.mi_path")
  nvo_path="aa"+os.sep+"bb"
  return nvo_path

def mi_path2():
  print("Segundo PATH")

if __name__=="__main__":
  print ("... MODULOS_001 MAIN")
  print (mi_path())

