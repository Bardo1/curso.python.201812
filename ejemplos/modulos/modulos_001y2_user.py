#!/usr/bin/env python3

import sys
# sys.path.append('/ufs/guido/lib/python')
sys.path.append('.')

import modulos_001

print ("------")
print( 'dir(modulos_001)')
print( dir(modulos_001))

print ("------")
print( ("modulos_001.mi_path"))
print( modulos_001.mi_path )

print ("------")
print( "modulos_001.mi_path()")
print( modulos_001.mi_path() )


print ("---------------")

# -----------------------------------------

from modulo002.modulo002a.ejmod002a import valor
import  modulo002.modulo002a.ejmod002a  as ej

print ("valor")
print (valor)
print (ej.cadena)













