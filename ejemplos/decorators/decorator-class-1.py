# PythonDecorators/my_decorator.py
class my_decorator(object):

    def __init__(self, f):
        print("@ inside my_decorator.__init__()")
        f() # Prove that function definition has completed

    def __call__(self):
        print("@ inside my_decorator.__call__()")

print("* while loading module")

@my_decorator
def aFunction():
    print("- inside aFunction()")

print("* Finished decorating aFunction()")

if __name__=="__main__":
    print("* before exec")
    aFunction()
