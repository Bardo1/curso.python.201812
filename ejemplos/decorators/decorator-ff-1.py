# PythonDecorators/entry_exit_function.py
def entry_exit(f):
    def new_f():
        print("@ Entering", f.__name__)
        f()
        print("@ Exited", f.__name__)
    return new_f

@entry_exit
def func1():
    print("- inside func1()")

@entry_exit
def func2():
    print("- inside func2()")

if __name__=="__main__":
    print("* inicio")
    func1()
    func2()
    print(func1.__name__, "!!! la ff es reemplazada por el decorador, que la invoca !!!")
    print("* final")
