EJERCICIO SUGERIDO

Aplicación en línea de comando.

Funcionalidad:
 + capaz de separar palabras
 + genera diccionario (unicidad)
 + entrega el diccionario, a pedido
 + puede responder si una palabra fue utilizada
 
 Adicionalmente:
  - la fuente puede ser parametrizada:
    - stdin
    - archivo
    - sugerir (db, api)
  - la salida puede indicarse del mismo modo
  - puede programarse en una función, OOP, FP
  - puede implementarse en módulos

Versión 0:
 - entrada: Hardcoded
 - salida: StdOut
 
 Versión 1:
 - entrada: StdIn 
 - salida: StdOut
 
 
Sugerencias:
 - Realizarlo en varias iteraciones
 - Documentar funcionalidades
 - Generar tests:
    - doctest
    - pytest
    
    