#!/b

# source ~/.bashrc
# 
# makevirtualenv PY2Pandas
# 
# workon PY2Pandas
# pip install pandas

import pandas as pd

y_2018 = '2018.csv'
y_2017 = '2017.csv'

y_all = 'all.csv'

csv_2018 = pd.read_csv(y_2018)
csv_2017 = pd.read_csv(y_2017)
csv_all = pd.read_csv(y_all)

accs_2018 = csv_2018.ACC
customer_2018 = csv_2018.CUSTOMER
spend_2018_csv = csv_2018.SPEND

accs_2017 = csv_2017.ACC
customer_2017 = csv_2017.CUSTOMER
spend_2017 = csv_2017.SPEND

all_accounts = csv_all.ACC
all_customers = csv_all.CUSTOMER

spend_2018 = []
spend_2017 = []

customers = []

for acc, customer in zip(all_accounts, all_customers):
    customers.append([acc, customer, 0, 0])

for acc, spend in zip(accs_2018, spend_2018_csv):
    spend_2018.append([acc, spend])


for acc, spend in zip(accs_2017, spend_2017_csv):
    spend_2017.append([acc, spend])


for i in range(len(customers)):
    for j in range(len(spend_2018)):
        if str(spend_2018[j][0]) in customers[i][0]:
            customers[i][2] = spend_2018[j][1]
        
    for j in range(len(spend_2017)):
        if str(spend_2017[j][0]) in customers[i][0]:
            customers[i][3] = spend_2017[j][1]

unique_customers = []

for elem in customers:
    if elem not in unique_customers:
        unique_customers.append(elem)

df = pd.dataframe(unique_customers, columns=["ACC", "CUSTOMER", "SPEND_2018", "SPEND_2017"],)
df.to_csv('Customer_Spenders.csv', index=False)





