

Objetivo:

Generar y comparar alternativas de codificación según lo especificado.

Especificación:

- Generar un informe de saldos por cuenta
- Generar un informe de saldos por fecha

En cuanto a fecha, puede abrirse:
- hasta una fecha específica (saldos)
- en un rango de fechas (estudio de movimientos de la cuenta)
- mensual general (ídem anterior)
- de un mes solicitado (estudio de la cuenta)

- Entradas:
    Registros de movimientos: id, fecha, tipo, cuenta, importe.

- Salidas:
    Registro de cantidad y saldo total de movimientos.

- Datos:
    - tipo: indica "FC", "DB", "CR", "CO" (factura, débito, crédito, cobranza)
    - fecha: 'YYYY-mm-dd'
