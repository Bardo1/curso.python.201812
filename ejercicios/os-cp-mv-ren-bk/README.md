

Objetivo:

Desarrollar un programa capaz de realizar una o más de las sgte operaciones, en iteraciones sucesivas.

- verificar existencia de la carpeta de origen, si corresponde
- verificar existencia de la carpeta destino, si corresponde
- verificar existencia del archivo origen, si corresponde
- verificar existencia del archivo destino, si corresponde
- realizar la copia del origen en el destino
- mover origen al destino
- renombrar el origen según el nombre destino, con o sin validaciones
- realizar respaldo del origen en el destino, en formato a elegir
