#!/bin/sh

l=$(<<LLLL

xlsxwriter
gspread
oauth2client
PyOpenSSL
google-api-python-client
google-auth-httplib2
google-auth-oauthlib
#google-api-python-client==1.7.8
#google-auth-httplib2==0.0.3
#google-auth-oauthlib==0.2.0

LLLL
)

for i in $l; do
	echo "### $l"
	pip install $i
	pip3 install $i
done

##xlsxwriter
##gspread
##oauth2client
##PyOpenSSL
##google-api-python-client
##google-auth-httplib2
##google-auth-oauthlib
##google-api-python-client==1.7.8
##google-auth-httplib2==0.0.3
##google-auth-oauthlib==0.2.0
