#!/bin/env python3

"""

https://www.youtube.com/watch?v=7I2s81TsCnc


Acceso a GoogleSheets compartido.

    Leer GSheet "Curso.python.Ej.1"

    Analizar:
        math_normal_distrib
        departments
        id_parent
        primes

    Guardar en: "Curso.python.Ej.1-out"
    - Sheet1: Organizar por departamento

    Columna a analizar: "departments"

    Para cada departamento, generar sheet
    Colocar cada empleado en las hojas correspondientes a su lista de departamentos
    En cada hoja, ordenar alfabético-descendente por [Apellido, Nombre]




INSTALAR:
    pip install -r requirements.txt

-------------------------------
PROBLEMAS DE ESTA VERSION:

    raise APIError(response)
gspread.exceptions.APIError: {
  "error": {
    "code": 429,
    "message": "Quota exceeded for quota group 'WriteGroup' and limit 'USER-100s' of service 'sheets.googleapis.com' for consumer 'project_number:151381342838'.",
    "status": "RESOURCE_EXHAUSTED",
    "details": [
      {
        "@type": "type.googleapis.com/google.rpc.Help",
        "links": [
          {
            "description": "Google developer console API key",
            "url": "https://console.developers.google.com/project/151381342838/apiui/credential"
          }
        ]
      }
    ]
  }
}

-------------------------------

"""

DBG = not False  # únicamente para debug
DBG_RECS4TEST = 4  # registros a analizar en debug

Comparto = {
    "carpeta": {
        "owner": "ejmvar@gmail.com",
        "other": "ejmv.rosario@gmail.com",
    },
    "docnames": [
        "curso-1",
        "curso-2",
    ],
    "GoogleDeveloperConsole": [
        {"Google_developer_console": "https://console.developers.google.com",
         "sheet": "enable",
         "drive": "enable",
         "credentials": {
             "create": 1,
             # "api": "GoogleDrive API",
             "api": "GoogleSheets API",
             "calling-API-from": "non-UI",
             "data-accessing": "Application data",
             "use this API with App Engine or Compute Engine": "no",

             "Create a service account name": "curso-python",
             "role": [
                 "Datastore",
                 "Cloud Datastore User",
             ],
             "Service account ID": [
                 "curso-python",
                 "@ember-library-app-31391.iam.gserviceaccount.com",
             ],
             # "service_account-key": 1,
             "key-type": "json",

             "filename": "ember-library-app-8afec4ede7ac.json",

             "file-contents": """{
  "type": "service_account",
  "project_id": "ember-library-app-31391",
  "private_key_id": "8afec4ede7acf9523eb9da9ab3add381609a5171",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDml0D0jl2P+/th\nCWzQyhwRVTiscqDHXpIH4OjXmJ4KTq3ZXAhVAPQd3+ADMYUTkyaDQ/oMED+O2ZT0\nTy5AoxiOAHmzA7yDYialaTN8s0WL9JohWlHXKG1IU8G/asBQhp2IS2dOAwxoGN19\nuHBb05OX4UYkDJMcKEP7ScgjCpso1WA/BT854smM+w3kUABXXTND/aAauytJVepw\nsfcBR9eQvaAlCfP5Nv6x+E2B2e3Zfhmn0dk6StvjWWwS7I+J2WVUVL7bVlasx0nx\n9r1CQcrOtLtz5wQEo8xA897lVFeuTRQHi/6HqXm897Vwm4Gi6qf+DNg1ZtU5NJdF\nGkObvjuVAgMBAAECggEALR2h3Vdvcejs1Z8I9c7db/y7Em55C5jyvGziUbBwF3cu\nZ40sqij+Gkw/oZBSR7KuQdeOeEbDkLj8flJxq77dYocwSHiuLQyKoCl1o+vVax0n\n/OeVhKHJw/FzQnbp+rdj4hNSun45MrLOvzbTcPvz8C7o0MvPCp7MMkHM0SJVmkbK\nMHZOgF2DmOwgbRWpJ/Rmc+99NazWiMut7ADXlVf2Yfgbb22Jn1KqnLXGDuM0hTPt\ns7Wvya4Errsw4nbinILvdTDi6BxP51eqVMvMCyy/Y8kSYuRRC09Rh8kbeDBoaKgY\niQTMTUyvjVE8CoWxBoXrQuKpxF3D61of3XbGbjhLHwKBgQD2yCib3PNmoTw2Y3zW\nfYaTAtjk8n0ZsOoRTFbsoN3ZZX738szHw9C3Hpv95sETfH/Uu6kM1G5jmcutzFmJ\n+lHegVfUbrINNBH0Jj/4GKtg+Jw1b7LiBnHsy1vg9e9NBY3RIE69l4M/+DBIyeUG\nQ3AJganvblexegB+coP1CT+T3wKBgQDvNETdm3Hl0nIKMytsa+4yR/PiUKkTkbqQ\nw/1fmmQmCkr6LnI367knu+vsU79K0UKH+Myj9i5wRDSjOFqZcVn8wRzMN7DBOli5\ndhoULJOTvfLyNPvc6/1UFQ+G1qus7sEDsS15mLaYSE/jKvIbLYIHxlPxSsHKNkDc\nNRdXm0E/CwKBgCnQ9tiaUwAj+BD7QK8TrBcDF3+L+1i8Gp19wwjrpUvuH9qh4MNE\n0fwFisc2L6c+pGaGufl7kYz9BX9Yrh9sNC0w25Iag78rAJPPW22NuJSA6+8+EyCr\nmOhX+SSgZ8E0XLh/8fe8UbgoNP5Qf6dVDAoAV6xDlUZ0dtr2SpfdzlEfAoGBAI43\nEUqABLOS7uJJkeOgN7oFNWa0dhGAbt9q5tVUQX6sV+9uddHxf9+zxox3LiAZUejt\nbx18XBe7NWBlPNhFoC650ZlI8vmu6tk48kBUUFlicXK3NZcfZzn6W4TbVK44NOEx\nEcxdkxHuzDBPdCncYJamv3mR7K2AnAdQOVSIpb4JAoGBAIkvZJOCpPlaSVUe1pK4\nPw8VX/E2uVL/Zq+MXoOSilN+iaEZIfeQvaQ3A5AVPIFvsEfkOTxo+B7m6Ch8byRy\nKhwY58QsEhNPuL4vGqV/fXpCsn+iIK8WlSCAVfSEX75hrYpXovIfzdzkavJu1Eck\nIW+vesy+1mwi6qNgcvJCvUNr\n-----END PRIVATE KEY-----\n",
  "client_email": "curso-python@ember-library-app-31391.iam.gserviceaccount.com",
  "client_id": "109841406086365859986",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/curso-python%40ember-library-app-31391.iam.gserviceaccount.com"
}""",

             "page": """
             
             Credentials

 
Create credentials to access your enabled APIs. For more information, see the authentication documentation.

API keys
Name	Creation date	Restrictions	Key	
 Server key (auto created by Google Service)	Jan 3, 2017	None	AIzaSyBvGDMJc4lv_OX-So79rJ9Xf-mRIN7f4vU 	 
 Browser key (auto created by Google Service)	Jan 3, 2017	None	AIzaSyDJvfnnMQy9YrjJexstoo6nHj4_BBpfR7U 	 
OAuth 2.0 client IDs
Name	Creation date	Type	Client ID	
Web client (auto created by Google Service)	Jan 3, 2017	Web application	151381342838-s37gca9m4jivls4jmattu0ng45lssn2r.apps.googleusercontent.com 	  
Service account keys Manage service accounts
ID	Creation date	Service account	
8afec4ede7acf9523eb9da9ab3add381609a5171	Mar 20, 2019	curso-python	

             """
         },
         },
    ],
    "share-with": {"client-email": "curso-python@ember-library-app-31391.iam.gserviceaccount.com"}
}

# pip install gspread oauth2client PyOpenSSL

# not working


# --------------
#
# https://www.youtube.com/watch?v=7I2s81TsCnc
#
# Learn more about gspread: https://github.com/burnash/gspread
#
# --------------

# https://www.twilio.com/blog/2017/02/an-easy-way-to-read-and-write-to-a-google-spreadsheet-in-python.html

# vER: http://www.indjango.com/access-google-sheets-in-python-using-gspread/
# Instalar mediante:
# pip install gspread
# pARA USAR oaUT2:
# pip install python-gflags oauth2client
# pip install gspread oauth2client

import gspread
from oauth2client.service_account import ServiceAccountCredentials

print(gspread)
print(ServiceAccountCredentials)

# Obtengo los "departamentos" que figuran en un registro (línea) de ha hoja
# dept_list_for_rec = lambda rIn: [x.strip() for x in rIn['departments'].split(',')]
dept_list_for_rec = lambda registro: [x.strip() for x in registro['departments'].split(',')]

if __name__ == "__main__":
    my_sheet = "Curso.python.Ej.1"
    my_sheetOut = "Curso.python.Ej.1-out"

    credentials_fname = "ember-library-app-8afec4ede7ac.json"

    scope = [
        'https://spreadsheets.google.com/feeds',
        'https://www.googleapis.com/auth/drive',
    ]

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        credentials_fname,
        scope,
    )

    gc = gspread.authorize(credentials)
    wks = gc.open(my_sheet).sheet1
    wkOut = gc.open(my_sheetOut)

    # print(wks.get_worksheet(1).get_all_records())
    all_records = wks.get_all_records()
    all_headers = dept_list_for_rec(all_records[0])

    if DBG: print(all_records)

    if DBG: print('Registros hallados: {} : {}'.format(len(all_records), all_records))

    # for i, r in enumerate(all_records):
    #     if DBG: print(i, r["departments"])
    #     dpt_lst = r["departments"].split(',')
    #     print(dpt_lst)
    #
    #     print(wkOut)
    #
    #     if i > DBG_RECS4TEST:
    #         break

    print("Hojas en la salida: ...")
    wkOut_worksheets = wkOut.worksheets()
    print("wkOut_worksheets:", wkOut_worksheets)

    if DBG: print("Eliminación de las hojas en la salida")
    for wi, w in enumerate(wkOut_worksheets):
        print("*wi {wi} - wid: {w.id} - w: {w} - w.title: {w.title}")
        try:
            r = w.clear()
            print("- CLEARED:", wi, w)
        except BaseException as exc:
            print("EXC:", exc)
        finally:
            pass

        try:
            wkOut.del_worksheet(w)
            print("DELETED:", wi, w)
        except BaseException as exc:
            print("-EXC:", exc)
        finally:
            pass

    print("Collecting 'departments' : start ...")
    all_departments = []
    all_departments_Set = set()

    # for rIn in wks.get_all_records():
    for rIn in all_records:
        # dept_list_for_rec
        sheet_deps = dept_list_for_rec(rIn)
        # sheet_deps = ([x.strip() for x in rIn['departments'].split(',')])
        print("sheet_deps:          {}: {}".format(len(sheet_deps), sheet_deps))
        all_departments_Set = all_departments_Set.union(set(sheet_deps))
        print("all_departments_Set: {}: {}".format(len(all_departments_Set), all_departments_Set))

    print("-" * 40)
    print("all_departments_Set: {}: {}".format(len(all_departments_Set), all_departments_Set))

    print("Preparo los HEADERS: ...")
    all_headers = all_records[0].keys()
    print("-    OK los HEADERS: {}: {}".format(len(all_headers), all_headers))

    # raise StopIteration

    # print("all_departments_Set:     {} - {}".format(len(all_departments_Set), all_departments_Set))
    print("Generando las hojas: ...")
    for h in all_departments_Set:
        if not h.strip():
            print("- ignoring EMPTY:", h)
            continue
        try:
            print("- try adding:", h)
            # !!!!! w.clear
            w = wkOut.add_worksheet(h, 0, 0)
            # w = wkOut.add_worksheet(h, 1, 1)
            print("- OK: added:", h)
        except Exception as exc:
            print("- EXC: adding:", h)
            w = wkOut.worksheet(h)
            pass
        print("Insert titles ...")

        # w.add_rows(1)
        # w.insert_row([1, 2, 3], 1)
        # w.insert_row([1, 2, 3], 1)
        # w.insert_row([1, 2, 3], 1)
        w.insert_row(list(all_headers), 1)

        # raise StopIteration

        # all_departments.extend([x.strip() for x in rIn['departments'].split(',')])

    print("! OK: Hojas generadas + Headers colocados")
    countAppendedRecs = 0
    for rNro, rIn in enumerate(all_records):
        # TODO: para los departamentos del registro
        # depts = [x.strip() for x in rIn['departments'].split(',')]

        # for dept in depts:
        # MAL # for dept in all_departments:
        # for dept in [x.strip() for x in rIn['departments'].split(',')]:
        # TODO: recorro los departamentos DEL REGISTRO!!
        for dept in dept_list_for_rec(rIn):
            print("- p/c/dept del registro: {}".format(dept))

            # TODO: para los departamentos NO VACIOS
            if not dept.strip():
                print("- salteo dept EMPTY: {}".format(dept))
                continue

            # TODO: elijo la hoja del departamento
            try:
                w = wkOut.worksheet(dept)

            except Exception as exc:
                print("EXC: selecting hoja '{}'\n{}".format(dept, exc))
                raise exc

            # TODO: inserto el registro
            try:
                print("- Insertando registro: ...\n{}".format(rIn))
                v = tuple(rIn.values())
                print("-- registro formateado: {}: {}".format(rNro, v))
                w.append_row(v)
                print("--- countAppendedRecs: {} try ...".format(countAppendedRecs))
                print("--- countAppendedRecs: {} OK!".format(countAppendedRecs))
                countAppendedRecs += 1
                # # w.append_row( tuple( rIn.values() ) )
                # # # w.append_row(rIn.values())
                # #
                # # # w.append_row(["a", "b", "c"])
                # # # w.append_row([1, 2, 3], 2)
                # # # w.append_row([11, 12, 13], 2)
                # # # w.append_row([21, 22, 23], 2)
                # # # # # w.append_row([1, 2, 3], 1)
                # # # # # w.insert_row([1, 2, 3], 1)
                # # # # # w.insert_row([1, 2, 3], 0)
                # # # # # w.insert_row(rIn.values(), 0)
                # # # # w.insert_row(rIn.values(), 1)
                # # # # # w.add_rows(rIn.values())
                # # # # # w.insert_row(rIn.values())
                # raise StopIteration
                # w.insert_row(rIn)
                # # w.add_rows(rIn)

            except Exception as exc:
                print("EXC: inserting record en hoja: '{}'\n{}".format((dept, exc)))

            pass

        # TODO: verificar:
        # TODO: qué pasa con celdas vacías

    print("FINALIZADO OK: !!")
    print("."*44)
    print("Departamentos = Hojas: {}: {}".format( len(all_departments), all_departments))
    print("."*44)
    print("Registros = Personas: {}".format( rNro))
    print("."*44)
    print("Inserciones totales: {}".format( countAppendedRecs))
    print("."*44)
    # raise StopIteration
    # # ===== ===== ===== ===== ===== ===== ===== ===== ===== =====
    #
    # # for rIn in wks.get_all_records():
    # for rIn in all_records:
    #     # # r_depts=rIn['departments']
    #     # # print(r_depts)
    #     # # r_depts_lst=r_depts.split(',')
    #     # # print(r_depts_lst)
    #     # #
    #     # # r_depts_lst=[x.strip() for x in r_depts_lst]
    #     # # print(r_depts_lst)
    #     # r_depts_lst=[x.strip() for x in rIn['departments'].split(',')]
    #     # print(r_depts_lst)
    #     # all_departments.extend(r_depts_lst)
    #     # print("= all_departments: ", all_departments)
    #     # all_departments.extend([x.strip() for x in rIn['departments'].split(',')])
    #
    #     all_departments.extend([x.strip() for x in rIn['departments'].split(',')])
    #     print("!= all_departments: ", all_departments)
    #
    #     all_departments_Set.union(([x.strip() for x in rIn['departments'].split(',')]))
    #     all_departments_Set = set(([x.strip() for x in rIn['departments'].split(',')]))
    #     print("!= all_departments_Set: ", all_departments_Set)
    #
    #     all_departments_Set.union(set([x.strip() for x in rIn['departments'].split(',')]))
    #
    #     print("= all_departments: ", len(all_departments), all_departments)
    #     print("= all_departments_Set: ", len(all_departments_Set), all_departments_Set)
    #
    # for t in all_departments_Set:
    #     try:
    #         w = wkOut.add_worksheet(t, 1, 1)
    #     except Exception as exc:
    #         print("EXC: adding:", t)
    #         w = wkOut.worksheet(t)
    #         pass
    #     w.insert_row([1, 2, 3], 0)
    #
    # #
    # #
    # #     for title in all_departments:
    # #         try:
    # #             w = wkOut.worksheet(title)
    # #         except Exception as exc:
    # #             print("NO HALLO:", title, exc)
    # #             try:
    # #                 w = wkOut.add_worksheet(title, 1, 1)
    # #                 print("worksheet added  is   : ", w)
    # #                 w = wkOut.worksheet(title)
    # #                 print("worksheet search added: ", w)
    # #
    # #                 w.append_row(rIn.keys())
    # #
    # #             except Exception as exc:
    # #                 print("NO AGREGO:", title, exc)
    # #                 raise exc
    # #                 break
    # #
    # #         # w.append_row(rIn)
    # #         w.insert_row(rIn)
    # #
    # #         # w = wkOut.worksheets().  .add_worksheet(title)
    # #         # w = wkOut.add_worksheet(title)
    # #         print("==FIN== ", title, w)
    # #         # w.append_row(rIn)
    # #     break
    # #
    # # # for wi, w in enumerate(wkOut_worksheets):
    # # #     break
    # # #     print("*1 w     :", wi, ": ", w.title)
    # # #     print("*2 w     :", ": ", w)
    # # #     print("*3 w.id  :", ": ", w.id)
    # # #
    # # #     # print("/wkOut_worksheets[", wi, "].title")
    # # #     # print(wkOut_worksheets[wi].title)
    # # #     try:
    # # #         all_recs = w.get_all_records()
    # # #     except BaseException as exc:
    # # #         print("!!! ", exc)
    # # #         all_recs = []
    # # #         print("!!!  asumo VACÍO !!!")
    # # #
    # # #     print(wi, w.id, w.title, w, "all_recs")
    # # #     print(all_recs)
    # # #     print("-" * 33)
    # # #     r = w.clear()
    # # #     print(r, "CLEARED")
    # # #     print("-" * 33)
    # # #     wkOut_worksheets.remove(w)
    # # #     print("-" * 33)
    # # #
    # # #     # print("/wkOut_worksheets[", wi, "].get_all_records()")
    # # #     # print(wkOut_worksheets[wi].get_all_records())
    # # #     # print("/wkOut_worksheets[", wi, "].clear()")
    # # #     # print(wkOut_worksheets[wi].clear())
    # # #
    # # #     # try:
    # # #     #     if "AAA" in wkOut_worksheets:
    # # #     #         print("Hallada..........")
    # # #     #     wkoNew = wkOut.add_worksheet("AAA", 1, 1)
    # # #     # except BaseException as exc:
    # # #     #     print("--- ", exc)
    # # #     #     print("--- ", exc.error)
    # # #     #     print("--- ", exc["error"])
    # # #     #     print("--- ", exc["error"]["code"])
    # # #     #
    # # #     #     if exc["error"]["code"]==400:
    # # #     #         print("No neceito agregar SHEET EXISTENTE :[{0}]".format("AAA"))
    # # #     #
    # # #     # wkoNew.insert_row(r, i)
    # # #     # wkOut.add_worksheet("AAA", 2, 2)
    # # #     # # wkOut.worksheet()
