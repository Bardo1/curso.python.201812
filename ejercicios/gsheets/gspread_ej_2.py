#!/bin/env python3

"""
Acceso a GoogleSheets compartido.



pip install -r requirements.txt

"""

Comparto = {
    "carpeta": {
        "owner": "ejmvar@gmail.com",
        "other": "ejmv.rosario@gmail.com",
    },
    "docnames": [
        "curso-1",
        "curso-2",
    ],
    "GoogleDeveloperConsole": [
        {"Google_developer_console": "https://console.developers.google.com",
         "sheet": "enable",
         "drive": "enable",
         "credentials": {
             "create": 1,
             # "api": "GoogleDrive API",
             "api": "GoogleSheets API",
             "calling-API-from": "non-UI",
             "data-accessing": "Application data",
             "use this API with App Engine or Compute Engine": "no",

             "Create a service account name": "curso-python",
             "role": [
                 "Datastore",
                 "Cloud Datastore User",
             ],
             "Service account ID": [
                 "curso-python",
                 "@ember-library-app-31391.iam.gserviceaccount.com",
             ],
             # "service_account-key": 1,
             "key-type": "json",

             "filename": "ember-library-app-8afec4ede7ac.json",

             "file-contents": """{
  "type": "service_account",
  "project_id": "ember-library-app-31391",
  "private_key_id": "8afec4ede7acf9523eb9da9ab3add381609a5171",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDml0D0jl2P+/th\nCWzQyhwRVTiscqDHXpIH4OjXmJ4KTq3ZXAhVAPQd3+ADMYUTkyaDQ/oMED+O2ZT0\nTy5AoxiOAHmzA7yDYialaTN8s0WL9JohWlHXKG1IU8G/asBQhp2IS2dOAwxoGN19\nuHBb05OX4UYkDJMcKEP7ScgjCpso1WA/BT854smM+w3kUABXXTND/aAauytJVepw\nsfcBR9eQvaAlCfP5Nv6x+E2B2e3Zfhmn0dk6StvjWWwS7I+J2WVUVL7bVlasx0nx\n9r1CQcrOtLtz5wQEo8xA897lVFeuTRQHi/6HqXm897Vwm4Gi6qf+DNg1ZtU5NJdF\nGkObvjuVAgMBAAECggEALR2h3Vdvcejs1Z8I9c7db/y7Em55C5jyvGziUbBwF3cu\nZ40sqij+Gkw/oZBSR7KuQdeOeEbDkLj8flJxq77dYocwSHiuLQyKoCl1o+vVax0n\n/OeVhKHJw/FzQnbp+rdj4hNSun45MrLOvzbTcPvz8C7o0MvPCp7MMkHM0SJVmkbK\nMHZOgF2DmOwgbRWpJ/Rmc+99NazWiMut7ADXlVf2Yfgbb22Jn1KqnLXGDuM0hTPt\ns7Wvya4Errsw4nbinILvdTDi6BxP51eqVMvMCyy/Y8kSYuRRC09Rh8kbeDBoaKgY\niQTMTUyvjVE8CoWxBoXrQuKpxF3D61of3XbGbjhLHwKBgQD2yCib3PNmoTw2Y3zW\nfYaTAtjk8n0ZsOoRTFbsoN3ZZX738szHw9C3Hpv95sETfH/Uu6kM1G5jmcutzFmJ\n+lHegVfUbrINNBH0Jj/4GKtg+Jw1b7LiBnHsy1vg9e9NBY3RIE69l4M/+DBIyeUG\nQ3AJganvblexegB+coP1CT+T3wKBgQDvNETdm3Hl0nIKMytsa+4yR/PiUKkTkbqQ\nw/1fmmQmCkr6LnI367knu+vsU79K0UKH+Myj9i5wRDSjOFqZcVn8wRzMN7DBOli5\ndhoULJOTvfLyNPvc6/1UFQ+G1qus7sEDsS15mLaYSE/jKvIbLYIHxlPxSsHKNkDc\nNRdXm0E/CwKBgCnQ9tiaUwAj+BD7QK8TrBcDF3+L+1i8Gp19wwjrpUvuH9qh4MNE\n0fwFisc2L6c+pGaGufl7kYz9BX9Yrh9sNC0w25Iag78rAJPPW22NuJSA6+8+EyCr\nmOhX+SSgZ8E0XLh/8fe8UbgoNP5Qf6dVDAoAV6xDlUZ0dtr2SpfdzlEfAoGBAI43\nEUqABLOS7uJJkeOgN7oFNWa0dhGAbt9q5tVUQX6sV+9uddHxf9+zxox3LiAZUejt\nbx18XBe7NWBlPNhFoC650ZlI8vmu6tk48kBUUFlicXK3NZcfZzn6W4TbVK44NOEx\nEcxdkxHuzDBPdCncYJamv3mR7K2AnAdQOVSIpb4JAoGBAIkvZJOCpPlaSVUe1pK4\nPw8VX/E2uVL/Zq+MXoOSilN+iaEZIfeQvaQ3A5AVPIFvsEfkOTxo+B7m6Ch8byRy\nKhwY58QsEhNPuL4vGqV/fXpCsn+iIK8WlSCAVfSEX75hrYpXovIfzdzkavJu1Eck\nIW+vesy+1mwi6qNgcvJCvUNr\n-----END PRIVATE KEY-----\n",
  "client_email": "curso-python@ember-library-app-31391.iam.gserviceaccount.com",
  "client_id": "109841406086365859986",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/curso-python%40ember-library-app-31391.iam.gserviceaccount.com"
}""",

             "page": """
             
             Credentials

 
Create credentials to access your enabled APIs. For more information, see the authentication documentation.

API keys
Name	Creation date	Restrictions	Key	
 Server key (auto created by Google Service)	Jan 3, 2017	None	AIzaSyBvGDMJc4lv_OX-So79rJ9Xf-mRIN7f4vU 	 
 Browser key (auto created by Google Service)	Jan 3, 2017	None	AIzaSyDJvfnnMQy9YrjJexstoo6nHj4_BBpfR7U 	 
OAuth 2.0 client IDs
Name	Creation date	Type	Client ID	
Web client (auto created by Google Service)	Jan 3, 2017	Web application	151381342838-s37gca9m4jivls4jmattu0ng45lssn2r.apps.googleusercontent.com 	  
Service account keys Manage service accounts
ID	Creation date	Service account	
8afec4ede7acf9523eb9da9ab3add381609a5171	Mar 20, 2019	curso-python	

             """
         },
         },
    ],
    "share-with": {"client-email": "curso-python@ember-library-app-31391.iam.gserviceaccount.com"}
}

# pip install gspread oauth2client PyOpenSSL

# not working


# --------------
#
# https://www.youtube.com/watch?v=7I2s81TsCnc
#
# Learn more about gspread: https://github.com/burnash/gspread
#
# --------------

# https://www.twilio.com/blog/2017/02/an-easy-way-to-read-and-write-to-a-google-spreadsheet-in-python.html

# vER: http://www.indjango.com/access-google-sheets-in-python-using-gspread/
# Instalar mediante:
# pip install gspread
# pARA USAR oaUT2:
# pip install python-gflags oauth2client
# pip install gspread oauth2client

import gspread
from oauth2client.service_account import ServiceAccountCredentials

print(gspread)
print(ServiceAccountCredentials)

if __name__ == "__main__":
    my_sheet = "Curso.python.Ej.1"

    credentials_fname = "ember-library-app-8afec4ede7ac.json"

    scope = [
        'https://spreadsheets.google.com/feeds',
        'https://www.googleapis.com/auth/drive',
    ]

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        credentials_fname,
        scope,
    )

    gc = gspread.authorize(credentials)
    wks = gc.open(my_sheet).sheet1

    # print(wks.get_worksheet(1).get_all_records())
    all_records = wks.get_all_records()
    print(all_records)

    print( 'Registros hallados: %s' % ( len(all_records) ) )

    for i,r in enumerate(reversed(all_records)):
        print(i, r )
